#!/bin/bash
################################################################################
################################################################################
################################### REQUIRE ####################################
#################################################################################
## This script clone a git project provided in argument and run composer in   ##
## it. It then copy ~/.ssh to current directory. It was designed for use in   ##
## Nexway's softgallery container tools.                                      ##
################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                            ##                                                                            
################################################################################
################################################################################
# Colors -----------------------
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
clone=0
# -------------------------------
clone_func () {
    git clone git@github.com:$1.git dev || git clone https://github.com/$1.git dev
}
# Removing existing code --------
int=${1:-0}
if [ -d dev ]
  then
    if [ $int -eq 0 ]
    then
        echo -e "$red Are you sure you want to execute the require tasks ?"
        echo -e "$red This will erase all the dev file. Make sure you didn't modify files in it."
        echo -e "$red [y/N] $basic"
        read -r -p "" response
        case $response in
            [yY][eE][sS]|[yY])
                rm -rf dev
                ;;
            *)
                echo -e "$green Dev will be updated$basic"
                cd dev && git pull
                cd ..
                clone='1'
                ;;
        esac
    else
        echo -e "$green Dev will be updated$basic"
        cd dev && git pull
        cd ..
        clone='1'
    fi
fi
# -------------------------------
# Cloning git project -----------
[ $clone -eq 0 ] && clone_func $2
# -------------------------------
# Running composer install ------
docker-compose -f docker-compose.composer.yml up
# -------------------------------
# Copiing ~/.ssh ----------------
cp -ru $HOME/.ssh $PWD
# -------------------------------
