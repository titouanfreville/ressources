#!/bin/bash

# Local Variables --------------------------------------------------------------
# ### COLORS ### #
blue="\\033[1;34m"
basic="\\033[0;39m"
# ------------------------------------------------------------------------------
MESSAGE="Hello small head :)"
echo
echo -en "${blue}${MESSAGE}${basic}"
echo
