#!/bin/bash
#################################################################################
#################################################################################
################################# TEST_REQUIRE ##################################
#################################################################################
## This script is used to test correction of local environment for Nexway's   ##
## softgallery container tool. It will check version of software required,     ##
## correctness of ssh access and login for private docker registry.            ##
#################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                             ##                                                                            
#################################################################################
#################################################################################
# Local Variables --------------------------------------------------------------
RETURN_CODE=0
# ### COLORS ### #
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
blue="\\033[1;34m"
# ### ### #
# ### VERSION REQUIRED ### #
DOCKER_REQUIRE="1.10.0"
COMPOSE_REQUIRE="1.7.1"
ME=$(whoami)
VERSION=0
# ### ### #
# ### OPTION DEFAULT VALUES ### #
quiet=1
interactive=0
debug=1
watching=1
env_file=
# ### ### #
# ------------------------------------------------------------------------------
# Version check functions ------------------------------------------------------
version_comp () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 0
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

test_version_comp () {
    version_comp $1 $2
    case $? in
        0) op='>=';;
        *) op='<';;
    esac
    if [[ $op = '<' ]]
    then
        echo -e "$red FAIL: Your version is older than require.,  '$1', '$2' $basic"
        return 1
    else
        echo -e "$green Pass: '$1 $op $2'. If greater, please report issue when not working. $basic"
        return 0
    fi
}
# ------------------------------------------------------------------------------
# Command Functions ------------------------------------------------------------
##################### INSTALLATIONS AND VERSIONS TESTS #########################
docker_ver () {
  echo "Checking requirement"
  echo -e "##################### INSTALLATIONS AND VERSIONS TESTS #########################"
  # Test Docker installation -----------------------------------------------------
  echo
  echo -e "$blue Checking docker installation .... $basic"
  VERSION=$(docker version --format '{{.Server.Version}}')
  if [ $? -eq 0 ]
  then
    test_version_comp $VERSION $DOCKER_REQUIRE
    if [ $? -eq 0 ]
    then
      echo -e "$green Docker well installed $basic"
    else
      echo -e "$red Please update Docker from https://docs.docker.com/engine/installation/ $basic"
      RETURN_CODE=1
    fi
  else
    echo -e "$red Please Install docker from https://docs.docker.com/engine/installation/ or make it run without sudo. Read the full doc :) $basic"
    RETURN_CODE=1
  fi
  echo
  VERSION=0
  # ------------------------------------------------------------------------------
  # Test Docker compose installation ---------------------------------------------
  echo -e "$blue Checking docker-compose installation .... $basic"
  VERSION=$(docker-compose version --short)
  if [ $? -eq 0 ]
  then
    echo
    test_version_comp $VERSION $COMPOSE_REQUIRE
    if [ $? -eq 0 ]
    then
      echo -e "$green Docker well installed $basic"
    else
      echo -e "$red Please update Docker compose from https://docs.docker.com/compose/install/ $basic"
      RETURN_CODE=1
    fi
  else
    echo -e "$red Please Install docker compose from https://docs.docker.com/compose/install/ $basic"
    RETURN_CODE=1
  fi
  echo
  VERSION=0
  # ------------------------------------------------------------------------------
  echo
  VERSION=0

  if [ $RETURN_CODE -eq 1 ]
  then
    echo -e "$red They are some problems with your installation, please fix it before trying again$basic"
    return 1
  else
    echo -e "$basic ################################################################################"
    echo
    echo -e "$blue Installation seems good. Setting up."
  fi
  echo
}
# ------------------------------------------------------------------------------
################################################################################
##################### SETTING UP ###############################################
set_env () {
  clear
  docker pull registry.nexway.build/whiptails:1.0
  if [ ! -f .env ]
    then
      cp -f .env.dist .env
  fi

  cp -f .env .env.old

  docker-compose -f docker-compose.whiptails.yml run whiptails
  if [ $RETURN_CODE -eq 1 ]
  then
    echo -e "$red An error occur during setup. Please correct it before running again.$basic"
    exit 1
  else
    echo -e "$basic ################################################################################"
    echo
    echo -e "$blue Setting done. Now checking that it will work.$basic"
  fi
  echo
  clear
}
################################################################################
##################### SET UP TESTS #############################################
test_env () {
  int=${1:-0}
  source .env
  echo -e "$basic ##################### SET UP TESTS #############################################"
  echo
  # Check right on .composer -----------------------------------------------------
  echo -e "$blue Checking if you are owner of ~/.composer ... $basic"
  OWNER=$(ls -ld ~/.composer | awk 'NR==1 {print $3}') > /dev/null 1> /dev/null 2> /dev/null
  if [[ "$OWNER" = "$ME" ]]
  then
    echo -e "$green You own ~/.composer. $basic"
  else
    echo -e "$red Check the right on ~/.composer."
    RETURN_CODE=1
  fi
  echo
  # ------------------------------------------------------------------------------
  # Check right on .composer -----------------------------------------------------
  echo -e "$blue Checking if you are owner of $RSYNC_DEST_PATH ... $basic"
  OWNER=$(ls -ld $RSYNC_DEST_PATH | awk 'NR==1 {print $3}') > /dev/null 1> /dev/null 2> /dev/null
  if [[ "$OWNER" = "$ME" ]]
  then
    echo -e "$green You own $RSYNC_DEST_PATH. $basic"
  else
    echo -e "$red You are not owner of $RSYNC_DEST_PATH."
    echo -e "$blue Checking if you have writting rights on $RSYNC_DEST_PATH ... $basic"
    touch $RSYNC_DEST_PATH/testing > /dev/null 1> /dev/null 2> /dev/null
    if [ $? -eq 0 ]
      then
        echo -e "$green You have good permissions on $RSYNC_DEST_PATH. $basic"
      else 
        echo -e "$red You need to have write permission on $RSYNC_DEST_PATH. $basic"
        RETURN_CODE=1
    fi
  fi
  echo
  # ------------------------------------------------------------------------------
  # Check if you can log into preprod by key -------------------------------------
  echo -e "$blue Checking if you have set-up a key for preprod rsync ... $basic"
  ssh -i /home/$ME/.ssh/$RSYNC_KEY_NAME -o BatchMode=yes $RSYNC_USERNAME@192.168.200.254 'exit' > /dev/null 1> /dev/null 2> /dev/null
  if [ $? -eq 0 ]
  then
    echo -e "$green You have well configure preprod connection.$basic"
  else
    if [ $int -eq 0 ]
      then 
      echo -e "$blue Do you want to make the key now ?"
      echo -e "$blue [Y/n] $basic"
      read -r -p "" response
      case $response in
           [nN]|[nN][nN][Oo] )
             echo -e "$red Please set up a key to connect on preprod following : https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2 (replace IP by preprod ;) )"
             RETURN_CODE=1
             ;;
          *)
            echo -e "$red A key is going to be create in ~/.ssh/id_softgallery. If you want to interrupt the process, press Ctrl+C."
            echo -e "$red Waiting 10 seconds ..."
            sleep 10s
            ssh-keygen -t rsa -f /home/$ME/.ssh/$RSYNC_KEY_NAME -q -N ""
            echo "ssh-copy-id -i "/home/$ME/.ssh/$RSYNC_KEY_NAME.pub" $RSYNC_USERNAME@192.168.200.254"
            ssh-copy-id -i "/home/$ME/.ssh/id_softgallery.pub" $RSYNC_USERNAME@192.168.200.254
            ssh-add ~/.ssh/$RSYNC_KEY_NAME
      esac
      ssh -i /home/$ME/.ssh/$RSYNC_KEY_NAME -o BatchMode=yes $RSYNC_USERNAME@192.168.200.254 'exit' > /dev/null 1> /dev/null 2> /dev/null
      if [ $? -eq 0 ]
      then
        echo -e "$green You have well configure preprod connection.$basic"
      else
         echo -e "$red The configuration have failed. Check what is wrong (contact Ops team if needed)"
         RETURN_CODE=1
      fi
    else
     echo -e "$red Please set up a key to connect on preprod following : https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2 (replace IP by preprod ;) )"
     RETURN_CODE=1
    fi
  fi
  echo
  # ------------------------------------------------------------------------------
  # Check if you are log in Portus -----------------------------------------------
  echo -e "$blue Checking if you have logged in registry.nexway.build ... $basic"
  docker pull registry.nexway.build/hello-portus:1.0 2> /dev/null
  if [ $? -eq 0 ]
  then
    docker run registry.nexway.build/hello-portus:1.0
  else
    echo -e "$red You are not log in registry.nexway.build. Now running : docker login registry.nexway.build$basic"
    docker login registry.nexway.build
    if [ $? -eq 0 ]
      then
        docker run registry.nexway.build/hello-portus:1.0
      else
        echo -e "$red Can't make login. Fix the problem before trying again. $basic"
        RETURN_CODE=1
    fi
  fi
  echo
  # ------------------------------------------------------------------------------
  # Check git key ----------------------------------------------------------------
  echo -e "$blue Checking if you have setup a key for git (not required) ... $basic"
  ssh -T git@github.com > /dev/null 1> /dev/null 2> /dev/null
  if [ $? -eq 1 ]
  then
    echo -e "$green Git is ready$basic"
  else
    echo -e "$blue For a better experience, add a key for git following : https://help.github.com/articles/generating-an-ssh-key/$basic"
  fi
  echo
  echo -e "$basic ################################################################################"
  echo
  ################################################################################
  if [ $RETURN_CODE -eq 0 ]
  then
    echo -e "$green Seetings ok. Getting dependencies ... $basic"
  else
    echo -e "$red Error while testing settings. Check out what went wrong, correct and run me again :)$basic"
    return 1
  fi
}
################################################################################
##################### GETTING RESOURCES ########################################
# Getting softgallery code source ----------------------------------------------
get_resources () {
  ./scripts/require.sh $1 softgallery
  if [ $? -eq 0 ]
    then
      echo -e "$green Well getted softgallery repository.$basic"
    else
      echo -e "$red An error occur while getting softgallery resources, please checkout what whent wrong. $basic"
      RETURN_CODE=1
  fi
  # ------------------------------------------------------------------------------
  # Getting templates and i18n ---------------------------------------------------
  cp ~/.ssh .
  docker-compose -f docker-compose.rsync.yml run rsync
  if [ $? -eq 0 ]
  then
      echo -e "$green Well getted templates and i18n.$basic"
    else
      echo -e "$red An error occur while getting templates and i18n resources, please checkout what whent wrong. $basic"
      RETURN_CODE=1
  fi
  # ------------------------------------------------------------------------------
  # Quick configuration ----------------------------------------------------------
  echo -e "$blue Running small configuration steps ... $basic"  
  if [ $1 -eq 0 ]
  then
    ./scripts/quick_conf.sh
  else
    ./scripts/quick_conf_ni.sh
  fi
  if [ $? -eq 0 ]
  then
      echo -e "$green Well getted templates and i18n.$basic"
    else
      echo -e "$red An error occur while getting templates and i18n resources, please checkout what whent wrong. $basic"
      RETURN_CODE=1
      return 1
  fi
  if [ $RETURN_CODE -eq 0 ]
  then
    echo -e "$green All went well, you can go now :) $basic"
    echo
    exit 0
  fi
}
# ------------------------------------------------------------------------------
################################################################################
##################### RUNNING SERVER ###########################################
serve() {
  ./scripts/check_port.sh
  if [ $? -eq 1 ]
    then exit 1
  fi
  docker-compose up $1
}
################################################################################
##################### RUNNING TEST #############################################
phpunit() {
  w=${1:-0}
  if [ $w -eq 1 ]
    then echo "$PHPUNIT_TEST_FOLDER=${PHPUNIT_TEST_FOLDER:-tests} 1"
  fi
  cat .env
  docker-compose up $2
}
################################################################################
# ------------------------------------------------------------------------------
# Running process --------------------------------------------------------------
# ### Process Variables ### #
# Get options passed
TEMP=`getopt -o dhqye:f: --long --debug,help,non-interactive,quiet,env-file: -n 'Softgallery Tools Initialisation' -- "$@"`
# Help message to print for -h option (or when not providing correctly)
HELP_MESSAGE="Usage: ./**/init.sh [OPTIONS] [COMMANDS]

A quick way to configure Nexway's Softgallery container tools.
Not providing command will default on executing all process.

This script require getopt to run.

Options:
  -d, --debug             Debug. Run the script with debug information.
  -e                      Environment variable in string format as : \"NAME=VALUE\".
  -f, --env-file          Name of the environment file to use in the script. Default : .env.
  -h, --help              Print this help.
  -q, --quiet             Silencing scripts. Render testing and getting resources non interactive by default.
  -y, --non-interactive   Run process non interactively.
  -w, --watch             Make test task watch for modification.

Command:
  get_resources   Cloning Softgallery repository from NexwayGroup, rsync templates for SalesUnit. 
  serve           Running softgallery server.
  set_env         Run settings helper. Will not be run if you provided 
                  non interactive flag (-y,--non-interactive).
  test            Running php unit test on softgallery.
  test_docker     Check if you have well setted docker. 
  test_env        Testing that you configure correctly the container.
"
# Set tasks variables to false and quick_conf to interactive (dv : docker_verion, se : set_env, te : test_env, gr: ger_resources, i: interactivity)
dv=1; se=1; te=1; gr=1; i=0;
# ### ### #
################################################################################
##################### GETTING ARGS #############################################
# REQUIRE : getopt. Should be available on most bash terminal ##################
# ENSURE : args variable setted with value provided by user   ##################
################################################################################
[ $debug -eq 0 ] && echo "Provided arguments BEFORE Flag reading : $@"
eval set -- "$TEMP"
while true
do
  case "${1}" in
    -h|--help)
      echo "$HELP_MESSAGE"; exit 0;;
    -w|--watch)
      watching=0;shift;;
    -d|--debug)
      debug=0;shift;;
    -q|--quiet)
      quiet=0;shift;;
    -y|--non-interactive)
      interactive=1;shift;;
    -e)
      env_args+=($2);shift 2;;
    -f|--env-file)
      env_file=$2;shift 2;;
    --) shift; break;;
    *) echo "You provided a wrong option"; echo $HELP_MESSAGE; exit 1;;
  esac
done
################################################################################
##################### WELCOME :) ###############################################
./scripts/welcome.sh
################################################################################
##################### GETTING COMMANDS #########################################
# ENSURE : command var are setted to true if command was provided by user ######
# DEFAULT SETTING : If no command provided, will set all to true          ######
################################################################################
[ $debug -eq 0 ] && echo "Provided arguments AFTER flag reading : $@"
if [ $# -eq 0 ]
then
  dv=0; se=0; te=0; gr=0;
else
  for cmd in $@
  do
    [ $debug -eq 0 ] && echo "Read command : $cmd";
    case "$cmd" in
      test_docker) 
        [ $debug -eq 0 ] && echo "Test-Docker command readed" 
        dv=0
        [ $debug -eq 0 ] && echo "Should provide 0 :::: Actual : $dv"
        ;;
      set_env)
        [ $debug -eq 0 ] && echo "Set-Env command readed" 
        se=0
        [ $debug -eq 0 ] && echo "Should provide 0 :::: Actual : $se"
        ;;
      test_env) 
        [ $debug -eq 0 ] && echo "Test-Env command readed"
        te=0
        [ $debug -eq 0 ] && echo "Should provide 0 :::: Actual : $te"
        ;;
      get_resources) 
        [ $debug -eq 0 ] && echo "Get-Resources command readed"
        gr=0
        [ $debug -eq 0 ] && echo "Should provide 0 :::: Actual : $gr"
        ;;
      *) echo "Unavailable command."; echo "$HELP_MESSAGE"; exit 1;;
    esac
  done
fi
################################################################################
##################### INTERRACTIVE SETUP #######################################
# ENSURE : Set up interactivity of the process according to user wishes ########
# DEFAULT SETTING : Process is interactive by default                   ########
################################################################################
if [ $interactive -eq 1 ]
  then
    [ $debug -eq 0 ] && echo "Non interractive mode activated."
    se=1;
    [ $debug -eq 0 ] && echo "Set environement should not be execute. Expect 1 ::::: Actual $se"
    i=1;
    [ $debug -eq 0 ] && echo "Interractive indice should be set to 1. Expect 1 ::::: Actual $se"
fi
################################################################################
##################### Add ENV_VAR from -e ######################################
# ENSURE : Env var provided from -e flags are added to wished env file #########
################################################################################
if [ ! -z $env_args ]
  then
    [ $debug -eq 0 ] && echo "Provided environement variable through -e tag. Values : ${env_args[@]}"
    for env in ${env_args[@]}
    do
      [ $debug -eq 0 ] && echo  "Readed arguments : $env"
      echo "$env" >> $env_file 
      [ $debug -eq 0 ] && echo  "Argument should be add to $env_file ::::: cat $env_file $(cat $env_file)"
    done
fi
################################################################################
##################### ENV_FILE SETTING #########################################
# ENSURE : Setted correctly env file on the wishes one #########################
################################################################################
if [ ! -z $env_file ]
  then
    [ $debug -eq 0 ] && echo  "Env file provided. Setting docker compose to use this file"
    deb_tmp=$(sed "s/#env_file: __ENV_FILE_NAME__/env_file: $env_file/g" *.yml)
    sed -i "s/#env_file: __ENV_FILE_NAME__/env_file: $env_file/g" *.yml
    [ $debug -eq 0 ] && echo "Shoul have replaced #env_file:.... with env_file value. :::::: $deb_tmp"
fi
################################################################################
##################### RUNNING COMMAND ##########################################
# ENSURE : Running wishes command quietly if required ##########################
################################################################################
if [ $quiet -eq 1 ]
  then
    [ $debug -eq 0 ] && echo "Running script as verbose. You should see output."
    [ $dv -eq 0 ] && docker_ver
    [ $se -eq 0 ] && set_env
    [ $te -eq 0 ] && test_env $i
    [ $gr -eq 0 ] && get_resources $i
  else
    source "./scripts/spinner.sh"
    [ $debug -eq 0 ] && echo "Running script as quiet. You should see spinners."
    # Checking docker
    [ $dv -eq 0 ] && start_spinner "Checking docker version"
    [ $dv -eq 0 ] && docker_ver > /dev/null
    [ $dv -eq 0 ] && stop_spinner $? "Checking docker version"
    # Setting env
    [ $se -eq 0 ] && set_env
    # Testing env
    [ $te -eq 0 ] && start_spinner "Checking that environment is correctly setted"
    [ $te -eq 0 ] && test_env 1 > /dev/null
    [ $te -eq 0 ] && stop_spinner $? "Checking that environment is correctly setted"
    # Getting resources
    [ $gr -eq 0 ] && start_spinner "Getting resources"
    [ $gr -eq 0 ] && get_resources 1 > /dev/null
    [ $gr -eq 0 ] && stop_spinner $? "Getting resources"
fi
# ------------------------------------------------------------------------------
