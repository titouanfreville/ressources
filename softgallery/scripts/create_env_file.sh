#!/bin/bash
#################################################################################
#################################################################################
################################ CREATE_ENV_FILE ################################
#################################################################################
## This script provide an interactive solution to set correctly the .env file  ##
## required in SOCIETY's softgallery container tools.                          ##
#################################################################################
## Author: FREVILLE Titouan <titouanfreville@hotmail.fr>                       ##                                                                               
#################################################################################
#################################################################################
# Local Variables --------------------------------------------------------------
# ### COLORS ### #
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
bblue="\\033[1;34m"
blue="\\033[0;34m"
# ### ### #
# ------------------------------------------------------------------------------
# Source wipthails script ------------------------------------------------------
source "whiptails"
# ------------------------------------------------------------------------------
#Generate .env and .env.old file if not exist ----------------------------------
if [ ! -f .env ]
  then
    cp -f .env.dist .env
fi

cp -f .env .env.old

# ------------------------------------------------------------------------------
# Get variables to update ------------------------------------------------------
source .env.old
OPTIONS_TABLE=(
  "BUILD_UID" "$BUILD_UID" "ON"
  "DEV_HOST" "$DEV_HOST" "ON"
  "RSYNC_KEY_NAME" "$RSYNC_KEY_NAME" "ON"
  "RSYNC_USERNAME" "$RSYNC_USERNAME" "ON"
  "SALES_UNIT_CODE" "$SALES_UNIT_CODE" "ON"
  "SALES_UNIT_TEMPLATE_PREFIX" "$SALES_UNIT_TEMPLATE_PREFIX" "ON"
  "DEV_PATH_CAROUSEL" "$DEV_PATH_CAROUSEL" "OFF"
  "DEV_PATH_HOME" "$DEV_PATH_HOME" "OFF"
  "DEV_PATH_PICTO" "$DEV_PATH_PICTO" "OFF"
  "DEV_PATH_PROD" "$DEV_PATH_PROD" "OFF"
  "DEV_MYSQL_HOSTNAME" "$DEV_MYSQL_HOSTNAME" "OFF"
  "DEV_MYSQL_PASSWORD" "$DEV_MYSQL_PASSWORD" "OFF"
  "DEV_MYSQL_USERNAME" "$DEV_MYSQL_USERNAME" "OFF"
)
RES=$(init_checklist "Quick_conf for softgallery Docker" "PARAMETERS" "Select parameters to update -- space to select, enter to validate." ".env.old" $OPTIONS_TABLE 23)
LIST="${RES//\"}"
# ------------------------------------------------------------------------------
# Operate modification ---------------------------------------------------------
read -r -a array <<< "$LIST"
for key in "${array[@]}"
do
  case "$key" in
    # REQUIRED MODIFICATIONS ------------------------------------------------------------------------------------------------------------------------
    # Buid UID 
    # Call for a single input box wipthails so you can prove BUILD UID.
    # DEFAULT VALUE :: $UID
    # ENSURE THAT PROVIDED UID is putted in .env
    "BUILD_UID" )
      # 16 40
      INPUT="$(init_inputbox "Update .env" "BUILD_UID" "Enter your uid" $UID)"
      sed -i "s|BUILD_UID=.*|BUILD_UID=$INPUT|g" .env && ERROR=0 || ERROR=1
      BUILD_UID=$INPUT
      ;;    
    # Dev HOST
    # Call for a single input box wipthails so you can prove DEV HOST.
    # DEFAULT VALUE :: $hostname
    # ENSURE THAT PROVIDED HOSTAME is putted in .env
    "DEV_HOST" )
      INPUT="$(init_inputbox "Update .env" "DEV_HOST" "Enter dev hostname" 16 40 $(hostname))"
      sed -i 's|DEV_HOST=.*|DEV_HOST='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_HOST=$INPUT
      ;;
    # Rsync KEY NAME
    # Call for a single input box wipthails so you can prove RSYNC KEY NAME.
    # DEFAULT VALUE :: id_softgallery
    # ENSURE THAT PROVIDED RSYNC_KEY_NAME is putted in .env
    "RSYNC_KEY_NAME" )
      INPUT="$(init_inputbox "Update .env" "RSYNC_KEY_NAME" "Enter your private key name" 16 40 id_softgallery)"
      sed -i 's|RSYNC_KEY_NAME=.*|RSYNC_KEY_NAME='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      RSYNC_KEY_NAME=$INPUT
      ;;
    # Dev Rsync USER NAME
    # Call for a single input box wipthails so you can prove RSYNC USER NAME.
    # DEFAULT VALUE :: $(whoami)
    # ENSURE THAT PROVIDED RSYNC_USERNAME is putted in .env
    "RSYNC_USERNAME" )
      INPUT="$(init_inputbox "Update .env" "RSYNC_USERNAME" "Enter Rsync User Name" 16 40 $(whoami))"
      sed -i 's|RSYNC_USERNAME=.*|RSYNC_USERNAME='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      RSYNC_USERNAME=$INPUT
      ;;
    # Sales UNIT CODE
    # Call for a single input box wipthails so you can prove SALES UNIT CODE.
    # ENSURE THAT PROVIDED SALES_UNIT_CODE is putted in .env
    "SALES_UNIT_CODE" )
      INPUT="$(init_inputbox "Update .env" "SALES UNIT NAME" "Enter sales unit name" 16 40 $SALES_UNIT_CODE)"
      sed -i 's|SALES_UNIT_CODE=.*|SALES_UNIT_CODE='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      SALES_UNIT_CODE=$INPUT
      ;;
    # Sales UNIT TEMPLATE PREFIX
    # Call for a single input box wipthails so you can prove SALES UNIT TEMPLATE PREFIX.
    # ENSURE THAT PROVIDED SALES_UNIT_TEMPLATE_PREFIX is putted in .env
    "SALES_UNIT_TEMPLATE_PREFIX" )
      # Initialise a countable variable to 0 so we can have the number of available template server in SGV3 
      i=0
      # Getting list of available template server name from SGV3.SERVER database. This list contain duplicates values
      while read field
      do
        tmp=${field%_*}
        Duplicates+=("${tmp:-1} ")
      done < \
      <(mysql -N -h $SERVER -u slave sgv3 --execute 'SELECT Name from Server where Templates_Server NOT LIKE "NULL";')
      # Remove duplicates from the list
      NODUPLICATES=$(echo "${Duplicates[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' ')
      OPTIONS=()
      i=0
      # Initialise an OPTION table witch contain all entry from NO DUPLICATES list of Server id
      for na in ${NODUPLICATES[@]}
      do
        OPTIONS+=($(( i++ )) "$na")
      done
      # Run wipthails script witch provide a selector for OPTIONS table. 
      CHOISE=$(init_select "Update .env" "SALES_UNIT_TEMPLATE_PREFIX" "Select the Template Prefix Name you need to retrieve." $OPTIONS 20 80)
      SERVER=${OPTIONS[$[2*CHOISE+1]]}
      sed -i 's|SALES_UNIT_TEMPLATE_PREFIX=.*|SALES_UNIT_TEMPLATE_PREFIX='"$SERVER"'|g' .env && ERROR=0 || ERROR=1
      ;;
    # -----------------------------------------------------------------------------------------------------------------------------------------------
    # Others ----------------------------------------------------------------------------------------------------------------------------------------
    # Dev PATH CAROUSEL
    # Call for a single input box wipthails so you can prove PATH CAROUSEL.
    # ENSURE THAT PROVIDED DEV_PATH_CAROUSEL is putted in .env
    "DEV_PATH_CAROUSEL" )
      INPUT="$(init_inputbox "Update .env" "DEV_PATH_CAROUSEL" "Enter absolute path to img carroussel. Empty if not needed" 16 40 $DEV_PATH_CAROUSEL)"
      sed -i 's|DEV_PATH_CAROUSEL=.*|DEV_PATH_CAROUSEL='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_PATH_CAROUSEL=$INPUT
      ;;
    # Dev PATH HOME
    # Call for a single input box wipthails so you can prove PATH HOME.
    # ENSURE THAT PROVIDED DEV_PATH_HOME is putted in .env
    "DEV_PATH_HOME" )
      INPUT="$(init_inputbox "Update .env" "DEV_PATH_HOME" "Enter absolute path to img home. Empty if not needed" 16 40 $DEV_PATH_HOME)"
      sed -i 's|DEV_PATH_HOME=.*|DEV_PATH_HOME='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_PATH_HOME=$INPUT
      ;;
    # Dev PATH PICTO
    # Call for a single input box wipthails so you can prove PATH PICTO.
    # ENSURE THAT PROVIDED DEV_PATH_PICTO is putted in .env
    "DEV_PATH_PICTO" )
      INPUT="$(init_inputbox "Update .env" "DEV_PATH_PICTO" "Enter absolute path to img picto. Empty if not needed" 16 40 $DEV_PATH_PICTO)"
      sed -i 's|DEV_PATH_PICTO=.*|DEV_PATH_PICTO='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_PATH_PICTO=$INPUT
      ;;
    # Dev PATH PROD
    # Call for a single input box wipthails so you can prove PATH PROD.
    # ENSURE THAT PROVIDED DEV_PATH_PROD is putted in .env
    "DEV_PATH_PROD" )
      INPUT="$(init_inputbox "Update .env" "DEV_PATH_PROD" "Enter absolute path to img prod. Empty if not needed" 16 40 $DEV_PATH_PROD)"
      sed -i 's|DEV_PATH_PROD=.*|DEV_PATH_PROD='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_PATH_PROD=$INPUT
      ;;
    # Dev MYSQL MYSQL HOSTNAME
    # Call for a single input box wipthails so you can prove DEV MYSQL HOSTNAME.
    # ENSURE THAT PROVIDED DEV_MYSQL_HOSTNAME is putted in .env
    "DEV_MYSQL_HOSTNAME" )
      INPUT="$(init_inputbox "Update .env" "DEV_MYSQL_HOSTNAME" "Enter absolute path to img prod. Empty if not needed" 16 40 $DEV_MYSQL_HOSTNAME)"
      sed -i 's|DEV_MYSQL_HOSTNAME=.*|DEV_MYSQL_HOSTNAME='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_MYSQL_HOSTNAME=$INPUT
      ;;
    # Dev MYSQL USERNAME
    # Call for a single input box wipthails so you can prove DEV MYSQL USERNAME.
    # DEFAULT VALUE :: $hostname
    # ENSURE THAT PROVIDED DEV_MYSQL_USERNAME is putted in .env
    "DEV_MYSQL_USERNAME" )
      INPUT="$(init_inputbox "Update .env" "DEV_MYSQL_USERNAME" "Enter absolute path to img prod. Empty if not needed" 16 40 $DEV_MYSQL_USERNAME)"
      sed -i 's|DEV_MYSQL_USERNAME=.*|DEV_MYSQL_USERNAME='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_MYSQL_USERNAME=$INPUT
      ;;
    # Dev MYSQL PASSWORD
    # Call for a single input box wipthails so you can prove DEV MYSQL PASSWOR.
    # DEFAULT VALUE :: $hostname
    # ENSURE THAT PROVIDED DEV_MYSQL_PASSWORD is putted in .env
    "DEV_MYSQL_PASSWORD" )
      INPUT="$(init_inputbox "Update .env" "DEV_MYSQL_PASSWORD" "Enter absolute path to img prod. Empty if not needed" 16 40 $DEV_MYSQL_PASSWORD)"
      sed -i 's|DEV_MYSQL_PASSWORD=.*|DEV_MYSQL_PASSWORD='"$INPUT"'|g' .env && ERROR=0 || ERROR=1
      DEV_MYSQL_PASSWORD=$INPUT
      ;;
     * )
       echo "DONE"
       ;;
  esac
done
# ------------------------------------------------------------------------------
