#!/bin/bash
################################################################################
################################################################################
################################## QUICK_CONF ##################################
################################################################################
## This script provide a non interactive solution to set correctly the        ##
## softgallery project for Nexway's softgallery container tools. It will copy ##
## files required and make the required Variable substitution through project.##
################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                            ##                                                                            
################################################################################
################################################################################
# Local Variables --------------------------------------------------------------
RETURN_CODE=0
FRC=0
DB_VAR=0
CONFIG_CLASS=0
ENV=0
RABBIT=0
# ### COLORS ### #
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
blue="\\033[1;34m"
# ### ### #
source .env
# ------------------------------------------------------------------------------
echo -e "Making a basic set up ----------------------------------------------------"
# Copy -------------------------------------------------------------------------
echo -e
echo -e "COPY #####################################################################"
# rabbit.json ------------------------------------------------------------------
echo -e "$blue Copying rabbit.json.dist as rabbit.json.$basic"
cp -f rabbit.json.dist rabbit.json

if [ $? -eq 0 ]
then
  echo -e "$green DONE$basic"
else
  echo -e "$red FAILED TO COPY rabbit.json.dist as rabbit.json $basic"
  echo -e "$basic"
  FRC=$[$FRC+1]
fi
echo
# ------------------------------------------------------------------------------
# Copy docker/dbvar.inc.dist ---------------------------------------------------
echo -e "$blue Copying docker/dbvar.inc.dist into include.$basic"
cp -f docker/dbvar.inc.dist dev/include/dbvar.inc

if [ $? -eq 0 ]
then
  echo -e "$green DONE$basic"
else
  echo -e "$red FAILED TO COPY docker/dbvar.inc.dist dev/include/dbvar.inc $basic"
  echo -e "$basic"
  FRC=$[$FRC+1]
fi
echo
# ------------------------------------------------------------------------------
# Copy code/secureCustomer/Config.class.php.dev --------------------------------
echo -e "$blue Copying code/secureCustomer/Config.class.php.dev into code/secureCustomer/Config.class.php.$basic"
  cp -f dev/code/secureCustomer/Config.class.php.dev dev/code/secureCustomer/Config.class.php

if [ $? -eq 0 ]
then
  echo -e "$green DONE$basic"
else
  echo -e "$red FAILED TO COPY code/secureCustomer/Config.class.php.dev$basic"
  echo -e "$basic"
  FRC=$[$FRC+1]
fi
echo
# ------------------------------------------------------------------------------
if [ $FRC  -eq 0 ]
then
  echo -e "$green Copy are dones.$basic"
else
  echo -e "$red $FRC copy failed, correct them before next steps$basic"
  echo -e "##########################################################################"
  exit 1
fi
echo -e "##########################################################################"
echo -e "$basic"
# ------------------------------------------------------------------------------
FRC=0
# Seding basics ----------------------------------------------------------------
echo -e "Setting ##################################################################"
RETURN_CODE=0
# Setting up AsMessage ------------------------------------------
cp -f docker/async.message.config.dist dev/config/async.message.config && RETURN_CODE=0 || RETURN_CODE=1
###### RETURN ERRORS OR DONT
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green async.message.config.dist Setted"
else
  echo -e "$red async.message.config.dist NOT SETTED"
  FRC=$[$FRC+1]
fi
echo -e "$basic"
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting up code/secureCustomer/Config.class.php ------------------------------
if [ $CONFIG_CLASS -eq 0 ]
then
  echo -e "$blue Setting up code/secureCustomer/Config.class.php$basic"

  sed -i "s/__DEV_MYSQL_USERNAME__/$DEV_MYSQL_USERNAME/" dev/code/secureCustomer/Config.class.php
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_HOSTNAME__/$DEV_MYSQL_HOSTNAME/" dev/code/secureCustomer/Config.class.php
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_PASSWORD__/$DEV_MYSQL_PASSWORD/" dev/code/secureCustomer/Config.class.php
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  ###### RETURN ERRORS OR DONT
  if [ $RETURN_CODE -eq 0 ]
  then
    echo -e "$green code/secureCustomer/Config.class.php Setted"
  else
    echo -e "$red Failed to set code/secureCustomer/Config.class.php"
    FRC=$[$FRC+1]
  fi

  echo -e "$basic"
fi
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting include/dbvar.inc ----------------------------------------------------
if [ $DB_VAR -eq 0 ]
then
  echo -e "$blue Setting up include/dbvar.inc$basic"

  sed -i "s/__DEV_MYSQL_USERNAME__/$DEV_MYSQL_USERNAME/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_HOSTNAME__/$DEV_MYSQL_HOSTNAME/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi
  sed -i "s/__DEV_MYSQL_PASSWORD__/$DEV_MYSQL_PASSWORD/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_USERNAME_SESSION__/$DEV_MYSQL_USERNAME/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_HOSTNAME_SESSION__/$DEV_MYSQL_HOSTNAME/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_PASSWORD_SESSION__/$DEV_MYSQL_PASSWORD/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  sed -i "s/__DEV_MYSQL_HOSTNAME_RO__/$DEV_MYSQL_HOSTNAME/" dev/include/dbvar.inc
  if [ $? -ne 0 ]
  then
    RETURN_CODE=1
  fi

  ###### RETURN ERRORS OR DONE
  if [ $RETURN_CODE -eq 0 ]
  then
    echo -e "$green include/dbvar.inc Setted"
  else
    echo -e "$red Failed to set include/dbvar.inc"
    FRC=$[$FRC+1]
  fi
fi
echo -e "$basic"
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting include/TfrCacheManager.class.php ------------------------------------
echo -e "$blue Setting up include/TfrCacheManager.class.php$basic"

sed -i "s/prep2\.telechargement\.fr/memcached/g" dev/include/TfrCacheManager.class.php
if [ $? -ne 0 ]
then
  RETURN_CODE=1
fi

###### RETURN ERRORS OR DONT
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green include/TfrCacheManager.class.php Setted"
else
  echo -e "$red Failed to set include/TfrCacheManager.class.php"
  FRC=$[$FRC+1]
fi
echo -e "$basic"
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting tests/include/Order/*.php --------------------------------------------
echo -e "$blue Setting up tests/include/Order$basic"

# FULL_HOSTNAME=" $DEV_HOST.telechargement.fr"
sed -i "s/\.prep\.websizing\.com/.$DEV_HOST/g" dev/tests/include/Order/*.php
if [ $? -ne 0 ]
then
  RETURN_CODE=1
fi

###### RETURN ERRORS OR DONT
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green tests/include/Order Setted"
else
  echo -e "$red Failed to set tests/include/Order"
  FRC=$[$FRC+1]
fi

echo -e "$basic"
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting tests/include/Order/BaseTestCase.class.php ---------------------------
echo -e "$blue Setting up tests/include/Order/BaseTestCase.class.php$basic"

sed -i "s/127\.0\.0\.1/selenium/g" dev/tests/include/Order/BaseTestCase.class.php
if [ $? -ne 0 ]
then
  RETURN_CODE=1
fi
sed -i "s/\.prep\.websizing\.com/. $DEV_HOST/g" dev/tests/include/Order/BaseTestCase.class.php
if [ $? -ne 0 ]
then
  RETURN_CODE=1
fi

###### RETURN ERRORS OR DONE
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green tests/include/Order/BaseTestCase.class.php Setted"
else
  echo -e "$red Failed to set tests/include/Order/BaseTestCase.class.php"
  FRC=$[$FRC+1]
fi

echo -e "$basic"
# ------------------------------------------------------------------------------
RETURN_CODE=0
# Setting include/auto_prepend_file.inc ----------------------------------
echo -e "$blue Setting up include/auto_prepend_file.inc$basic"

sed -i "s/prepgraylog/appradar/g" dev/include/auto_prepend_file.inc
if [ $? -ne 0 ]
then
  RETURN_CODE=1
fi

###### RETURN ERRORS OR DONE
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green include/auto_prepend_file.inc Setted"
else
  echo -e "$red Failed to set include/auto_prepend_file.inc"
  FRC=$[$FRC+1]
fi
# ------------------------------------------------------------------------------
echo -e "$basic"
# End Return -------------------------------------------------------------------
if [ $FRC  -eq 0 ]
then
  echo -e "$green Setting up according to env.$basic"
else
  echo -e "$red Setting failed, correct the $FRC issues before next steps$basic"
  echo -e "##########################################################################"
  exit 1
fi
echo -e "##########################################################################"
# ------------------------------------------------------------------------------

echo -e "$basic"
exit 0
