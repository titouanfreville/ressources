#!/bin/bash
################################################################################
################################################################################
################################### PHP UNIT ###################################
#################################################################################
## This script is used by the TEST container from Nexway's softgallery tools  ##
## container. It will run php unit for every test named AllTests.php.         ##
## You can provide him others variable so it will run for provided folders or ##
## files                                                                      ##
################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                            ##
################################################################################
################################################################################
green="\\033[1;32m"
red="\\033[1;31m"
blue="\\033[1;34m"
basic="\\033[0;39m"
blue="\\033[1;34m"


watcher () {
# script:  watch
# author:  Mike Smullin <mike@smullindesign.com>
# license: GPLv3
# description:
#   watches the given path for changes
#   and executes a given command when changes occur
# usage:
#   watch <path> <cmd...>
#
  path=$1
  shift
  cmd=$*
  sha=0
  update_sha() {
    sha=`ls -lR --time-style=full-iso $path | sha1sum`
  }
  update_sha
  previous_sha=$sha
  build() {
    echo -en " building...\n\n"
    $cmd
    echo -en "\n--> resumed watching."
  }
  compare() {
    update_sha
    if [[ $sha != $previous_sha ]] ; then
      echo -n "change detected,"
      build
      previous_sha=$sha
    else
      echo -n .
    fi
  }
  trap build SIGINT
  trap exit SIGQUIT

  echo -e  "--> Press Ctrl+C to force build, Ctrl+\\ to exit."
  echo -en "--> watching \"$path\"."
  while true; do
    compare
    sleep 1
  done
}

check_file()
{
  local home="/home/httpd/softgallery"
  # Test if pass arguments is already a test case.
  # TRUE : execute phpunit and exit
  if [[ -f "${1}" ]]
  then
    ./vendor/bin/phpunit --colors=always ${1}
    exit 0;
  fi
  # Go into evry part of the folder to find any test case in it.
  for file in "${1}"/*
  do
    # echo "File : ${file}"
    if [[ -d "${file}" ]]
    then
      check_file ${file}
    elif [[ -f "${file}" ]]
    then
      is_test="${file:${#i}-12}"
      if [[ "${is_test}" = "AllTests.php" ]]
      then
        echo >> ${home}/logs/phpunit_errors.logs
        echo -en "$blue Test on ${file} : ------------------------------------------------\n"
        echo -en "$red If empty, search in logs ;) means that nothing was good here $basic\n"
        echo -en "$blue Test on ${file} : ------------------------------------------------ $basic\n" >> ${home}/logs/phpunit_errors.logs
        ./vendor/bin/phpunit --coverage-html ${home}/coverage
        ./vendor/bin/phpunit --colors=always ${file} 2>> ${home}/logs/phpunit_errors.logs
        echo -en "$blue DONE ------------------------------------------------------------- $basic\n" >> ${home}/logs/phpunit_errors.logs
        echo >> ${home}/logs/phpunit_errors.logs
        echo -en "$blue ------------------------------------------------------------------ $basic\n"
      fi
    fi
  done
}

echo -en "$blue UNIT TESTING ON THE `date` ----------------------------------------------- $basic\n"
echo -en "$blue UNIT TESTING ON THE `date` ----------------------------------------------- $basic\n" >> /home/httpd/softgallery/logs/phpunit_errors.logs
echo >> /home/httpd/softgallery/logs/phpunit_errors.logs
watch={$2:-0}
CMD="check_file ${1}"
if [ $watch -eq 0 ]
  then
    echo "Watching ---------------------------------"
    watcher /home/httpd/softgallery $CMD
  else
    echo "Not Watching ------------------------------"
    $CMD
fi
