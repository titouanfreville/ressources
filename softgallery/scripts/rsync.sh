#!/bin/bash
#################################################################################
#################################################################################
##################################### RSYNC #####################################
#################################################################################
## This script is used by the RSYNC container of Nexway's Softgallery tools    ##
## container. It is used to get templates server id to retrieve for provided   ##
## SALES UNIT, basic templates, compiled version of i18n codes and some images.##
#################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                             ##                                                                            
#################################################################################
#################################################################################
# Local variables --------------------------------------------------------------
# ### Colors ### #
red="\\033[1;31m"
basic="\\033[0;39m"
# ### ###### ### #
# ### #HOME# ### #
# ### ###### ### #
HOME=/home/$RSYNC_USERNAME
# ### ###### ### #
# ------------------------------------------------------------------------------
# Checking and initializing database variables ---------------------------------
echo "environement is : "
env
echo "--------------------"
if [ -z "RSYNC_USERNAME" ] || [ -z "$RSYNC_MYSQL_HOSTNAME" ] || [ -z "$RSYNC_MYSQL_USERNAME" ] || [ -z "$RSYNC_MYSQL_DATABASE" ] || [ -z "$SALES_UNIT_TEMPLATE_PREFIX" ]
    then
        echo -e "$red A small devil passed here and erase your ENV_FILE, please check and correct it :O $basic"
        echo "Script exited with error code: 666"
        exit 666
fi
query="SELECT Server__ from Server where name like '${SALES_UNIT_TEMPLATE_PREFIX}%' and Server__ NOT LIKE 'NULL';"
# ------------------------------------------------------------------------------
# Spinner ----------------------------------------------------------------------
source "/scripts/spinner.sh"
function start_spinner {
    # ${RSYNC_USERNAME} : msg to display
    echo >> $2/rsync.logs
    echo "${RSYNC_USERNAME} -----------------------------------------" >> $2/rsync.logs
    echo >> $2/rsync_errors.logs
    echo "${RSYNC_USERNAME} -----------------------------------------" >> $2/rsync_errors.logs
    _spinner "start" "${1}" &
    # set global spinner pid
    _sp_pid=$!
    disown
}
function stop_spinner {
    echo "End $2 -------------------------------------" >> $3/rsync.logs
    echo "End $2 -------------------------------------" >> $3/rsync_errors.logs
    # ${RSYNC_USERNAME} : command exit status
    _spinner "stop" ${RSYNC_USERNAME} $_sp_pid
    unset _sp_pid
}
# ------------------------------------------------------------------------------
# Rsyncs templates for SALES UNIT ----------------------------------------------
# Opening new logs initialized with date value. 
echo " -------------------------------------- " >> ./rsync.logs
echo " -------------------------------------- " >> ./rsync_errors.logs
date >> /home/rsyncu/rsync.logs
date >> /home/rsyncu/rsync_errors.logs
# Getting templates id to retrieve.
if [ -z "$RSYNC_MYSQL_PASSWORD"]
    then
        res=$(mysql -N -h $RSYNC_MYSQL_HOSTNAME -u $RSYNC_MYSQL_USERNAME $RSYNC_MYSQL_DATABASE --execute "$query")
    else 
        res=$(mysql -N -h $RSYNC_MYSQL_HOSTNAME -u $RSYNC_MYSQL_USERNAME -p$RSYNC_MYSQL_PASSWORD $RSYNC_MYSQL_DATABASE --execute "$query")
fi
# For every ids acquired, rsync corresponding templates folder.
for ids in $res
do
    #Rsync private ----------------------------------------------
    start_spinner "rsync $ids templates" .
    rsync --delete -avzpog -e "ssh -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
    $RSYNC_IP:/home/httpd/softgallery/private/templates/$ids* data/templates  >> rsync.logs 2>> rsync_errors.logs
    stop_spinner $? "rsync $ids templates" .
    # -----------------------------------------------------------
done
# ------------------------------------------------------------------------------   
#Rsync common templates  -------------------------------------------------------
start_spinner "rsync 629 templates" .
rsync --delete -avzpog -e "ssh  -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
$RSYNC_IP:/home/httpd/softgallery/private/templates/629* data/templates >> rsync.logs 2>> rsync_errors.logs
stop_spinner $? "rsync 629 templates" .

start_spinner "rsync 724 templates" .
rsync --delete -avzpog -e "ssh -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
$RSYNC_IP:/home/httpd/softgallery/private/templates/724* data/templates >> rsync.logs 2>> rsync_errors.logs
stop_spinner $? "rsync 724 templates" .
# ------------------------------------------------------------------------------   
#Rsync i18n and images ---------------------------------------------------------
start_spinner "rsync caroussel images" .
rsync --delete -avzpog -e "ssh -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
$RSYNC_IP:/home/httpd/softgallery/img_carrousel data/img_carrousel >> rsync.logs 2>> rsync_errors.log
stop_spinner $? "rsync 724 templates" .

start_spinner "rsync img_picto images" .
rsync --delete -avzpog -e "ssh -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
$RSYNC_IP:/home/httpd/softgallery/img_picto data/img_picto >> rsync.logs 2>> rsync_errors.log
stop_spinner $? "rsync 724 templates" .

start_spinner "rsync i18n " .
rsync --delete -avzpog -e "ssh -o StrictHostKeyChecking=no -i $HOME/.ssh/$RSYNC_KEY_NAME" \
$RSYNC_IP:/home/httpd/softgallery/i18n/* data/i18n >> rsync.logs 2>> rsync_errors.log
stop_spinner $? "rsync i18n" .
# ------------------------------------------------------------------------------   
echo " -------------------------------------- " >> rsync_errors.logs
