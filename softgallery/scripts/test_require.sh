#!/bin/bash
#################################################################################
#################################################################################
################################# TEST_REQUIRE ##################################
#################################################################################
## This script is used to test correction of local environment for Nexway's   ##
## softgallery container tool. It will check version of software required,     ##
## correctness of ssh access and login for private docker registry.            ##
#################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                             ##                                                                            
#################################################################################
#################################################################################
# Local Variables --------------------------------------------------------------
RETURN_CODE=0
# ### COLORS ### #
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
blue="\\033[1;34m"
# ### ### #
# ### VERSION REQUIRED ### #
DOCKER_REQUIRE="1.10.0"
COMPOSE_REQUIRE="1.7.1"
ME=$(whoami)
RSYNC_NAME=$1
KEY_NAME=$2
VERSION=0
# ### ### #
# ------------------------------------------------------------------------------
# Others - Before --------------------------------------------------------------
echo "Checking requirement"
# ------------------------------------------------------------------------------
# Version check functions ------------------------------------------------------
version_comp () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 0
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

test_version_comp () {
    version_comp $1 $2
    case $? in
        0) op='>=';;
        *) op='<';;
    esac
    if [[ $op = '<' ]]
    then
        echo -e "$red FAIL: Your version is older than require.,  '$1', '$2' $basic"
        return 1
    else
        echo -e "$green Pass: '$1 $op $2'. If greater, please report issue when not working. $basic"
        return 0
    fi
}
# ------------------------------------------------------------------------------
##################### INSTALLATIONS AND VERSIONS TESTS #########################
echo -e "##################### INSTALLATIONS AND VERSIONS TESTS #########################"
# Test Docker installation -----------------------------------------------------
echo
echo -e "$blue Checking docker installation .... $basic"
VERSION=$(docker version --format '{{.Server.Version}}')
if [ $? -eq 0 ]
then
  test_version_comp $VERSION $DOCKER_REQUIRE
  if [ $? -eq 0 ]
  then
    echo -e "$green Docker well installed $basic"
  else
    echo -e "$red Please update Docker from https://docs.docker.com/engine/installation/ $basic"
    RETURN_CODE=1
  fi
else
  echo -e "$red Please Install docker from https://docs.docker.com/engine/installation/ or make it run without sudo. $basic"
  RETURN_CODE=1
fi
echo
VERSION=0
# ------------------------------------------------------------------------------
# Test Docker compose installation ---------------------------------------------
echo -e "$blue Checking docker-compose installation .... $basic"
VERSION=$(docker-compose version --short)
if [ $? -eq 0 ]
then
  echo
  test_version_comp $VERSION $COMPOSE_REQUIRE
  if [ $? -eq 0 ]
  then
    echo -e "$green Docker well installed $basic"
  else
    echo -e "$red Please update Docker compose from https://docs.docker.com/compose/install/ $basic"
    RETURN_CODE=1
  fi
else
  echo -e "$red Please Install docker compose from https://docs.docker.com/compose/install/ $basic"
  RETURN_CODE=1
fi
echo
VERSION=0
# ------------------------------------------------------------------------------
echo
VERSION=0

if [ $RETURN_CODE -eq 1 ]
then
  echo -e "$red They are some problems with your installation, please fix it before trying again$basic"
  exit 1
else
  echo -e "$basic ################################################################################"
  echo
  echo -e "$blue Installation seems good. Testing the set ups."
fi
echo
# ------------------------------------------------------------------------------
################################################################################
##################### SET UP TESTS #############################################
echo -e "$basic ##################### SET UP TESTS #############################################"
echo
# Check right on .composer -----------------------------------------------------
echo -e "$blue Checking if you are owner of ~/.composer ... $basic"
OWNER=$(ls -ld ~/.composer | awk 'NR==1 {print $3}') > /dev/null 1> /dev/null 2> /dev/null
if [[ "$OWNER" = "$ME" ]]
then
  echo -e "$green You own ~/.composer. $basic"
else
  echo -e "$red Check the right on ~/.composer."
  RETURN_CODE=1
fi
echo
# ------------------------------------------------------------------------------
# Check if you can log into preprod by key -------------------------------------
echo -e "$blue Checking if you have set-up a key for preprod rsync ... $basic"
ssh -i /home/$ME/.ssh/$KEY_NAME -o BatchMode=yes $RSYNC_NAME@ $SERVER 'exit' > /dev/null 1> /dev/null 2> /dev/null
if [ $? -eq 0 ]
then
  echo -e "$green You have well configure preprod connection.$basic"
else
  echo -e "$blue Do you want to make the key now ?"
  echo -e "$blue [Y/n] $basic"
  read -r -p "" response
  case $response in
       [nN]|[nN][nN][Oo] )
         echo -e "$red Please set up a key to connect on preprod following : https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2 (replace IP by preprod ;) )"
         RETURN_CODE=1
         ;;
      *)
        echo -e "$red A key is going to be create in ~/.ssh/id_softgallery. If you want to interrupt the process, press Ctrl+C."
        echo -e "$red Waiting 10 seconds ..."
        sleep 10s
        ssh-keygen -t rsa -f /home/$ME/.ssh/$KEY_NAME -q -N ""
        echo "ssh-copy-id -i "/home/$ME/.ssh/$KEY_NAME.pub" $RSYNC_NAME@ $SERVER"
        ssh-copy-id -i "/home/$ME/.ssh/id_softgallery.pub" $RSYNC_NAME@$SERVER
	      ssh-add ~/.ssh/$KEY_NAME
  esac
  ssh -i /home/$ME/.ssh/$KEY_NAME -o BatchMode=yes $RSYNC_NAME@ $SERVER 'exit' > /dev/null 1> /dev/null 2> /dev/null
  if [ $? -eq 0 ]
  then
    echo -e "$green You have well configure preprod connection.$basic"
  else
     echo -e "$red The configuration have failed. Check what is wrong (contact Ops team if needed)"
     RETURN_CODE=1
  fi
fi
echo
# ------------------------------------------------------------------------------
# Check git key ----------------------------------------------------------------
echo -e "$blue Checking if you have setup a key for git (not required) ... $basic"
ssh -T git@github.com > /dev/null 1> /dev/null 2> /dev/null
if [ $? -eq 1 ]
then
  echo -e "$green Git is ready$basic"
else
  echo -e "$blue For a better experience, add a key for git following : https://help.github.com/articles/generating-an-ssh-key/$basic"
fi
echo
echo -e "$basic ################################################################################"
echo
# ------------------------------------------------------------------------------
################################################################################
# Ohers - After ----------------------------------------------------------------
if [ $RETURN_CODE -eq 0 ]
then
  echo -e "$green All required installation are done ;) $basic"
  echo
  exit 0
fi

echo -e "$basic"
exit 1
# ------------------------------------------------------------------------------
