#!/bin/bash
#
#################################################################################
#################################################################################
################################### CMD.DIS.SH ##################################
#################################################################################
## This file is a model to specific manipulation that could be required by the ##
## Nexway's Haxe compiler container. It has to be used if you need to install ##
## specific library that are not include in the base images, also use it if    ##
## you need to make specific tasks BEFORE compiling.                           ##
#################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                             ##                                                                               
#################################################################################
#################################################################################
#
# This file is a Model for Nexway's HAXE Docker. It has to be used if you need to take actions before compiling. You can remove or add step. 
# You don't have to care about linux/windows syntax of haxe scripts as it should be taken care from entry point part. If you used others
# specific syntax, make sure to adapt them to linux way using sed or directly updating your files. 
# You have to copy this file to the root of the code you'll mount as a volume when running the container. Cf README.md
#
# Add specific library if you have. :)
echo ">>> Adding Library"
haxelib install __LIBRARY__ __VERSION__ && haxelib set  __LIBRARY__ __VERSION__
# Have fun and do what you want ;) 
echo ">>> DO Something if you want :p"
