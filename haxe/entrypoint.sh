#!/bin/bash
## ### COLORS ### #
red="\\033[1;31m"
basic="\\033[0;39m"
# ### ### #
if [ -f cmd.sh ]
  then
    echo ">>> Executing special tasks :O"
    ./cmd.sh
  else
    echo -e "$red You did not provide a Specific CMD.SH. Proceeding from basic libraries and replacement. $basic"
fi

cd /home/compile

echo ">>> Making File working with linux"
sed -i 's#\\#\/#g' *.hxml
sed -i 's/move/mv/g' *.hxml
sed -i 's/xcopy/rsync -avz/g' *.hxml
sed -i 's#\s\/Y\s\/D##g' *.hxml

echo ">>> Compiling"
haxe $@
