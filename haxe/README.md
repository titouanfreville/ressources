# HAXE Container
This container exist to help compilation process for HAXE. It can be use to compile any project. Neko library is include.

# How to use ?

**Make sure to run the containers using ` --user $(id -u)  ` parameter to have right on the created files`**

## From compiled image. 

- Move to your haxe project root.
- Get the `cmd.dist.sh` code from  `NexwayGroup/Docker`.
- Edit it with your own values if needed (if you have to make specific operations before compiling -- [cf How to create Haxe cmd](#how-to-create-haxe-cmd)) 
- Run `docker run --rm -it --name haxe  --user $(id -u) -v __PATH_TO_USEFUL_RESOURCE_FOR_SETUP__:/home/haxe -v __PATH_TO_SRC_TO_COMPILE__:/home/compile titouanfreville/haxe_compiler:1.1 FILE TO COMPILE`

## From this repository, building a new image

- Clone the current project : `git@github.com:NexwayGroup/Docker.git || git@github.com:NexwayGroup/Docker.git` to clone current project (will be referred as $DOCKER_CLONE)
- `cd $DOCKER_CLONE`
- `docker build -t haxe .`
- `cp cmd.dist.sh $YOUR_CODE_PATH/cmd.sh` if needed (if you have to make specific operations before compiling -- [cf How to create Haxe cmd](#how-to-create-haxe-cmd)) 
- `cd $YOUR_CODE_PATH`
- Run `docker run --rm -it --name haxe  --user $(id -u) -v __PATH_TO_USEFUL_RESOURCE_FOR_SETUP__:/home/haxe -v __PATH_TO_SRC_TO_COMPILE__:/home/compile haxe FILE TO COMPILE`

## Running with a watcher. (Process will then watch for modification in /home/compile)

Follow the base steps from Compiled images or Building a new one.
Run with the following command : 
`docker run --rm -it --name haxe  --user $(id -u) --entrypoint "watcher" -v __PATH_TO_USEFUL_RESOURCE_FOR_SETUP__:/home/haxe -v __PATH_TO_SRC_TO_COMPILE__:/home/compile haxe FILE TO COMPILE`

# How to create Haxe cmd

- **The file as to be name `cmd.sh` and be placed at the root of your project so the container so the container can find it (or at the root of the mount volume point if you mount only haxe files part).**
- **The following table provide minimal steps you can or have to use in the script. They have to be placed in a cmd function so they can be run.**
- **Remember that you cannot pass arguments to this function.** 

| What you have to do ?      | How ?                                                                                               | Required ?                   |
| -------------------------- | --------------------------------------------------------------------------------------------------- | ---------------------------- |
| Set specific library       | Add library entry to the script. Do it by running `haxelib install` and `haxelib set` commands      | No                           |
| Adapt code to linux style  | Add sed entry. Small issues are already taken care of (replaced \ with / and move/xcopy equivalent) | Yes if other things to adapt |
| Have fun with bash command | If you have any operation to take care of before compiling ~~                                       | No                           |
