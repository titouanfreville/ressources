# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :collector, Collector.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "mU6F+A2LkGDpOzHKaOJAmG49Zwtc4hoHS+jK+gULbRBvs2M9H/kU1Na73VE2Wd6h",
  render_errors: [view: Collector.ErrorView, accepts: ~w(json)],
  pubsub: [name: Collector.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :collector,
  aws_raw_stream:
    System.get_env("AWS_KINESIS_STREAM_NAME")
    || "bubbles-raw-dev",
  default_data_schema:
    System.get_env("DEFAULT_DATA_SCHEMA")
    || "default.json",
  schema_path:
    System.get_env("SCHEMA_PATH")
    || "schemas/",
  protocol_schema:
    System.get_env("PROTOCOL_SCHEMA")
    || "protocol.json",
  shard:
    System.get_env("KiNESIS_SHARD")
    || "shardId-000000000000"


# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure AWS
config :ex_aws,
  region: [{:system, "AWS_REGION"}, :instance_role],
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
  secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role]

config :ex_aws, :retries,
  max_attempts: [{:system, "AWS_KINESIS_MAX_ATTEMPTS"}, :instance_role]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
