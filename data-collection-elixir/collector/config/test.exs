use Mix.Config

config :collector, Collector.Endpoint,
  http: [port: 4001],
  server: false

config :collector,
  aws_raw_stream: "bubbles-test",
  default_data_schema: "default.json",
  schema_path: "schemas/",
  protocol_schema: "protocol.json",
  shard: "shardId-00000000"

config :logger, level: :error

config :ex_aws,
  json_codec: Test.JSONCodec,
  access_key_id: "",
  secret_access_key: "",
  region: "us-east-1"

config :ex_aws, :kinesis,
  scheme: "http://",
  host: "localstack",
  port: 4568

config :ex_aws, :retries,
  max_attempts: 2,
  base_backoff_in_ms: 10,
  max_backoff_in_ms: 10_000
