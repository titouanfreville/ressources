use Mix.Config

config :collector, Collector.Endpoint,
  http: [:inet6, port: 80],
  # https: [
  #   :inet6,
  #   port: 442,
  #   keyfile: System.get_env("SSL_KEY_PATH"),
  #   certfile: System.get_env("SSL_CERT_PATH")
  # ],
  url: [host: {:system, "HOST"}, port: 80],
  # url: [host: {:system, "HOST"}, ports: 443],
  # force_ssl: [hsts: true],
  cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger, level: :info

# Finally import the config/prod.secret.exs
# which should be versioned separately.
import_config "prod.secret.exs"


# Configure AWS
config :ex_aws,
region: [{:system, "AWS_REGION"}, :instance_role],
access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role]

config :ex_aws, :retries,
max_attempts: [{:system, "AWS_SQS_MAX_ATTEMPTS"}, :instance_role]
