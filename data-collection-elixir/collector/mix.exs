defmodule Collector.Mixfile do
  use Mix.Project

  def project do
    [
      app: :collector,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: cover()
    ]
  end

  def cover do
    [
      "coveralls": :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Collector, []},
      extra_applications: [:logger, :ex_aws, :hackney, :poison]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test) do
     ["lib", "web", "test/support", "test/controllers"]
  end

  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.0-rc"},
      {:phoenix_pubsub, "~> 1.0"},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
      {:ex_aws, "~> 1.0"},
      {:hackney, "~> 1.6"},
      {:sweet_xml, "~> 0.6.5"},
      {:ex_json_schema, "~> 0.5.4"},
      {:dogma, "~> 0.1", only: [:dev, :test]},
      {:bypass, "~> 0.7", only: :test},
      {:excoveralls, "~> 0.7", only: :test}
    ]
  end
end
