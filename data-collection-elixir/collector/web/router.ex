defmodule Collector.Router do
  @moduledoc """
  `Collector.Router` manage routing for the API.
  """
  use Collector.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Collector do
    pipe_through :api
    # endpoint to check server availability
    get "/health", CollectController, :health
    # endpoint to collect data from post method
    post "/collect", CollectController, :collect
  end
end
