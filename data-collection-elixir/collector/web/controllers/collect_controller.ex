defmodule Collector.CollectController do
  @moduledoc """
  The `Collector.CollectController`
  module is used to manage data collection
  from trackers.
  """
  use Collector.Web, :controller
  require Logger

  @protocol_error_message "Sent JSON object does not meet protocol requirement"
  @data_error_message "Sent JSON object does not meet data schema requirement"
  @parse_error_message "Could not parse values back to JSON"
  @kinesis_error_message "AWS Kinesis ERROR"
  @shard Application.get_env(:collector, :shard)

  # @health
  # Manage health endpoint.
  # @ensure return "Healthy" as a JSON response if server is running.
  @doc """
  The `health/0` function is used to manage the health endpoint of the API.
  It will return the string Healthy in a JSON body if the server is running.
  """
  def health(conn, _params) do
    json conn, "Healthy"
  end

  # @inverse_tuple_list
  # Inverse tuple order in a couple list.
  # @param list ('a, 'b) list list of couple
  # @return ('b, 'a) list containing all elements of initial list
  @doc false
  def inverse_tuple_list(list) do
    cond do
      list == [] -> []
      true ->
        tuple = hd list
        [{elem(tuple, 1),elem(tuple, 0)}] ++ inverse_tuple_list(tl list)
      end
    end

  # @validate
  # Perform data validation from JSON object according to a JSON schema
  # @params data map Map representing the JSON Object
  # @params schema map Map representing the JSON schema
  # @return {:ok, data} if object is valid, {:error, data, errors} else
  @doc false
  def validate(data, schema) do
    case ExJsonSchema.Validator.validate(schema, data) do
      :ok -> {:ok, data}
      {:error, errors} -> {
        :error, data,
        Enum.into(inverse_tuple_list(errors), %{})
      }
    end
  end

  # @decode_json_validation_schema
  # Try to decode data validation schema
  # @param json_schema {atom, string} acquired schema after file request
  # @return decoded validation schema
  # @return fallback on default schema on error
  @doc false
  def decode_json_validation_schema(json_schema) do
    if elem(json_schema, 0) == :ok do
      res = Poison.decode(elem(json_schema, 1))
      if  elem(res, 0) == :ok do
        elem(res, 1)
      else
        get_validation_schema()
      end
    else
      get_validation_schema()
    end
  end

  # @get_validation_schema
  # Get correct validation schema depending on parameters
  # @params params map map where to find data schema config
  # @return correct data schema depending on results
  @doc false
  defp get_validation_schema(%{"custom-data-schema-url" => val}) do
    res = :httpc.request(val)
    decode_json_validation_schema(res)
  end

  @doc false
  defp get_validation_schema(%{"data-schema" => val}) do
    res = File.read(Application.get_env(:collector, :schema_path) <> val)
    decode_json_validation_schema(res)
  end

  @doc false
  defp get_validation_schema(_param) do
    get_validation_schema()
  end

  @doc false
  defp get_validation_schema do
    default =
      Application.get_env(:collector, :schema_path)
      <> Application.get_env(:collector, :default_data_schema)

    default
    |> File.read!
    |> Poison.decode!
  end

  # @send_to_stream
  # Send data to Kinesis stream
  # @param conn connection
  # @param ok_json json to sent back if all is good
  # @param stream string stream in witch we send datas
  # @params object json string object to send to stream
  # @ensure correct data send to stream
  # @ensure log error if needed
  @doc false
  defp send_to_stream(conn, ok_json, stream, object) do
    Logger.debug stream
    # sending values to Kinesis stream
    request =
      stream
      |> ExAws.Kinesis.put_record(@shard, object)
      |> ExAws.request

    if elem(request, 0) == :ok do
      # though it should not,
      # some could be lost during
      # Kinesis process so we set return code to 202
      conn = put_status(conn, 202)
      json conn, ok_json
    else
      # if error occurs, send back the error message
      err = elem(request, 1)
      Logger.error(
        @kinesis_error_message <> " error: "
        # <> err
      )

      conn = put_status(conn, 502)
      json conn, %{id: @kinesis_error_message, message: "Message", error: err}
    end
  end

  # @process_error
  # Process error is a small funciton to generate error string to be logged
  # @param id string error ID
  # @param data_as_string string data failling
  # @param error json map map representing the error
  #        to send (will be encoded as json string)
  # @return string containing all error informations
  @doc false
  defp process_error(id, data_as_string, error) do
    root = id <> " " <> data_as_string <> " "
    encoded_error = Poison.encode(error)
    if elem(encoded_error, 0) == :ok do
      root <> elem(encoded_error, 1)
    else
      root <> "Unknown Error"
    end
  end

  # @collect
  # Perform the data validation and collection from tracker.
  # @params conn (phxdefined) connection
  # @params params map map of the parameters
  #         passed via the endpoint. Here, a
  #         map representing the JSON passed to POST method.
  # @ensure Add data correctly to Kinesis if data are valid.
  # @ensure Print as error and return as JSON response
  #         on wrong data or Kinesis communication errors.
  @doc """
  The `collect/2` function is the function used
  to manage JSON object received from trackers.
  It will make sure that the provided data are
  conform to the collector protocol schema then
  put the object into the Kinesis raw stream.
  If error happen during the validation or
  parsing process, the error will be logged.
  If error happen while posting information to
  the Kinesis streams, the API will send back an error message.
  """
  def collect(conn, params) do
    protocol =
      Application.get_env(:collector, :schema_path)
      <> Application.get_env(:collector, :protocol_schema, "protocol.json")

      protocol_validation_schema =
      protocol
      |> File.read!
      |> Poison.decode!

    new_params = Poison.encode(params)
    if elem(new_params, 0) == :ok do
      validate_json = validate(params, protocol_validation_schema)
      if elem(validate_json, 0) == :ok do
        process_collect(conn, params)
      else
        @protocol_error_message
        |> process_error(elem(new_params, 1), elem(validate_json, 2))
        |> Logger.error()

        conn = put_status(conn, 422)
        json conn, %{
          id: @protocol_error_message,
          data: elem(validate_json, 1),
          errors: elem(validate_json, 2)
        }
      end
    else
      @protocol_error_message
      |> process_error("Could not parse datas", "")
      |> Logger.error

      conn = put_status(conn, 422)
      json conn, %{id: "Could not parse received JSON object", data: params}
    end
  end

  @doc """
  @process_collect

  Process JSON validation and post correct message to Kinesis

  @param conn connection
  @param params json map map representing received JSON
  @ensure correct message is posted to Kinesis
  @ensure data going into error are logged with the error
  @send JSON to inform tracker how the process went
  """
  def process_collect(conn, params) do
    raw_stream = Application.get_env(:collector, :aws_raw_stream)
    data = params["data"]
    # get default validation schema as map
    validation_schema = get_validation_schema(params)
    # getting value back as a JSON string
    values = Poison.encode(data)
    if elem(values, 0) != :ok do
      # when we can't parse data to json (should not happen)
      @parse_error_message
      |> process_error("Could not encode datas", "")
      |> Logger.error

      conn = put_status(conn, 422)
      json conn, %{
        id: @parse_error_message,
        data: elem(values, 1),
        error: elem(values, 1)
      }
    else
      # validating json object against shema
      valid_values = validate(data, validation_schema)
      if elem(valid_values, 0) == :ok do
        send_to_stream(conn, %{
          id: "Ok",
          received: data
        }, raw_stream, elem(values, 1))
      else
        # when data doesn't comply with JSON schema
        @data_error_message
        |> process_error(elem(values, 1), elem(valid_values, 2))
        |> Logger.error

        conn = put_status(conn, 422)
        json conn, %{
          id: @data_error_message,
          data: elem(valid_values, 1),
          errors: elem(valid_values, 2)}
      end
    end
  end
end
