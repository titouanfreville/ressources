ExUnit.start

defmodule TestCollectController do
  @moduledoc false

  use ExUnit.Case, async: true
  use Plug.Test
  require Logger
  alias Collector.CollectController, as: Collect

  @moduletag :capture_log
  @stream Application.get_env(:collector, :aws_raw_stream)

  # TEST FUNCTIONS ----------------------------------------
  # @test inverse_tuple_list
  # Test that inverse tuple list correctly inverse the couple
  # elements from any lenght of tuple list
  test "inverse_tuple_list" do
    l = []
    r = Collect.inverse_tuple_list(l)
    assert r == l

    # Test on INT values
    l = [{1, 2}]
    r = Collect.inverse_tuple_list(l)
    assert r == [{2, 1}]

    l = [{1, 2}, {3, 4}]
    r = Collect.inverse_tuple_list(l)
    assert r == [{2, 1}, {4, 3}]

    l = [{1, 2}, {3, 4}, {5, 6}, {1, 2}, {5, 6}, {4, 3}]
    r = Collect.inverse_tuple_list(l)
    assert r == [{2, 1}, {4, 3}, {6, 5}, {2, 1}, {6, 5}, {3, 4}]

    # Test on Strings value
    l = [{"a", "b"}]
    r = Collect.inverse_tuple_list(l)
    assert r == [{"b", "a"}]

    l = [{"b", "a"}, {"f", "z"}]
    r = Collect.inverse_tuple_list(l)
    assert r == [{"a", "b"}, {"z", "f"}]

    l = [
      {"__test", "inverse"}, {"message", "error"},
      {"whoa", "c'est beau"}, {"piti", "wa"},
      {"ka", "yak"}, {"orni", "car"}
    ]
    r = Collect.inverse_tuple_list(l)
    assert r == [
      {"inverse", "__test"}, {"error", "message"},
      {"c'est beau", "whoa"}, {"wa", "piti"},
      {"yak", "ka"}, {"car", "orni"}
    ]
  end

  test "validate function should validate correct JSONS" do
    data = %{"key" => "value"}
    schema = %{}

    res = Collect.validate(data, schema)

    assert elem(res, 0) == :ok
    assert elem(res, 1) == data

    data = %{"key" => "value"}
    schema = %{
      "$schema" => "http://json-schema.org/draft-04/schema#",
      "properties" => %{
        "key" => %{
          "type" => "string"
        }
      }
    }

    res = Collect.validate(data, schema)

    assert elem(res, 0) == :ok
    assert elem(res, 1) == data
  end

  test "validate function should fail and return errors on wrong JSONS" do
    data = %{"key" => "value"}
    schema = %{
      "$schema" => "http://json-schema.org/draft-04/schema#",
      "properties" => %{
        "key" => %{
          "type" => "integer"
        }
      },
      "type" => "object"
    }

    res = Collect.validate(data, schema)

    assert elem(res, 0) == :error
    assert elem(res, 1) == data
    refute Map.equal?(%{}, elem(res, 2))
  end

  test "decode_json_schema should return a map representing a JSON schema" do
    schema = ~s({
      "$schema": "http://json-schema.org/draft-04/schema#",
      "properties": {
        "key": {
          "type": "integer"
        }
      },
      "type": "object"
    })


    res_schema = %{
      "$schema" => "http://json-schema.org/draft-04/schema#",
      "properties" => %{
        "key" => %{
          "type" => "integer"
        }
      },
      "type" => "object"
    }

    res = Collect.decode_json_validation_schema({:ok, schema})

    assert Map.equal?(res, res_schema)
  end

  test "decode_json_schema should fallback on default JSON schema on errors" do
    schema = ~s({
      "$schema": "http://json-schema.org/draft-04/schema#",
      "properties": {
        "key": {
          "type":
        }
      },
      "type": "object"
    })

    res = Collect.decode_json_validation_schema({:ok, schema})

    refute Map.equal?(res, %{})

    res1 = Collect.decode_json_validation_schema({:error, schema})

    refute Map.equal?(res1, %{})
    assert Map.equal?(res1, res)
  end

  # -------------------------------------------------------
  # TEST API RESPONSES ------------------------------------
  # Should always be true in test
  test "#health render Healthy message" do
    response =
      :get
      |> conn("/health")
      |> send_request

    assert response.status == 200
    assert response.resp_body == ~s("Healthy")
  end

  # Test collect endpoint.
  test "#collect render 200 when passing correct object" do

    # Create stream
    try do
       @stream
      |> ExAws.Kinesis.create_stream(1)
      |> ExAws.request!
    rescue
      _ -> Logger.error "stream exist"
    end

    # Minimal test object
    json_object = %{
      application: %{
        action: %{
          id: "test-action",
          name: "share"
        }
      },
      tracker: %{
        id: "test-tracker",
        timestamp: 12132134214
      }
    }

    send_json = %{data: json_object}

    expected = %{
      id: "Ok",
      received: json_object
    }

    response =
      :post
      |> conn("/collect", send_json)
      |> send_request

    assert response.resp_body == Poison.encode!(expected)
    assert response.status == 202
  end

  test "#collect should return 422 errors when entity is not correct" do
    # No Data key
    wrong_json = %{
      application: %{
        action: %{
          id: "test-action",
          name: "share"
        }
      },
      tracker: %{
        id: "test-tracker",
        timestamp: 12132134214
      }
    }
    response =
      :post
      |> conn("/collect", wrong_json)
      |> send_request()

    assert response.status == 422

    wrong_json = %{
      data: %{
        application: %{
          action: %{
            id: "test-action",
            name: "share"
          }
        },
        tracker: %{
          id: "test-tracker",
          timestamp: 12132134214
        }
      },
      "data-schema": "test"
    }

    response =
      :post
      |> conn("/collect", wrong_json)
      |> send_request()

    assert response.status == 422

    wrong_json = %{
      data: %{
        application: %{
          action: %{
            id: 12,
            name: "share"
          }
        },
        tracker: %{
          id: "test-tracker",
          timestamp: 12132134214
        }
      }
    }

    response =
      :post
      |> conn("/collect", wrong_json)
      |> send_request()

    assert response.status == 422
  end

  test "#collect should behave correctly when passing schema" do
    ok_json = %{
      data: %{
        application: %{
          action: %{
            id: "test-action",
            name: "share"
          }
        },
        tracker: %{
          id: "test-tracker",
          timestamp: 12132134214
        }
      },
      "data-schema": "1/X.json"
    }

    response =
      :post
      |> conn("/collect", ok_json)
      |> send_request()

    assert response.status == 202

    ok_json = %{
      data: %{
        application: %{
          action: %{
            id: "test-action",
            name: "share"
          }
        },
        tracker: %{
          id: "test-tracker",
          timestamp: 12132134214
        }
      },
      "data-schema": "unknow.json"
    }

    response =
      :post
      |> conn("/collect", ok_json)
      |> send_request()

    assert response.status == 202

    ok_json = %{
      data: %{
        application: %{
          action: %{
            id: "test-action",
            name: "share"
          }
        },
        tracker: %{
          id: "test-tracker",
          timestamp: 12132134214
        }
      },
      "data-schema": "unknow.json",
      "custom-data-schema-url": "random.fr"
    }

    response =
      :post
      |> conn("/collect", ok_json)
      |> send_request()

    assert response.status == 202
  end
  # -------------------------------------------------------
  # TEST HELPERS ------------------------------------------
  # @send_request
  # Make a request to the api for testing
  # @param conn connection call to make to the api
  # @return request response
  @doc false
  defp send_request(conn) do
    conn
    |> put_private(:plug_skip_csrf_protection, true)
    |> Collector.Endpoint.call([])
  end
  # -------------------------------------------------------
end
