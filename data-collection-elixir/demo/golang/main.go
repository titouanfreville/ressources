package main

import (
	"database/sql"
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"

	. "github.com/TheBubblesCompany/dmp-go-tracker"
	"github.com/TheBubblesCompany/dmp-go-tracker/application"
	"github.com/TheBubblesCompany/dmp-go-tracker/client"
	"github.com/TheBubblesCompany/dmp-go-tracker/user"
	"github.com/icrowley/fake"
	"github.com/jonboulle/clockwork"
	_ "github.com/lib/pq"
)

const (
	host     = "0.0.0.0"
	port     = 5432
	dbuser   = "bubbles-admin"
	password = "t6vwnj"
	db       = "bubbles-storage"
)

var (
	users           *[]user.User
	events          *[]application.Application
	viewEvents      *[]PageView
	overXEvents     *[]OverX
	clients         *[]client.Client
	trackerPayloads *[]TrackerPayload
	os              []string
	producer        []string
	emitter         *Emitter
	tracker         *Tracker
	psqlInfo        = fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, dbuser, password, db)
)

// Client DB  creation -----------
func initDB(client string) {
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Printf("No DB connection")
		return
	}
	defer db.Close()
	sqlStatement := `CREATE TABLE ` + client + ` (
		id SERIAL NOT NULL,
		"trackerId" character varying(255),
		application character varying(255),
		"trackingEventTime" timestamp without time zone,
		venue character varying(255),
		"venueCity" character varying(255),
		"venueCountry" character varying(255),
		"venueStreet" character varying(255),
		"venueZipcode" character varying(255),
		"userId" character varying(255),
		"platformProperties" character varying(255),
		"platformOs" character varying(255),
		"platformOsVersion" character varying(255),
		browser character varying(255),
		"browserVersion" character varying(255),
		"userDevice" character varying(255),
		"platformResolution" character varying(255),
		"platformUID" character varying(255),
		"platformProducer" character varying(255),
		"userCountry" character varying(255),
		decription character varying(255),
		"eventId" character varying(255),
		"eventName" character varying(255),
		"eventRoot" character varying(255),
		"userPayment" double precision,
		"userCurrency" character varying(255),
		"convertedUserPayment" double precision,
		"viewEventTitle" character varying(255),
		"viewEventRefer" character varying(255),
		"eventValueTimestamp" timestamp without time zone
	);
	ALTER TABLE ` + client + ` OWNER TO "bubbles-admin";
	ALTER TABLE ONLY ` + client + `
    ADD CONSTRAINT ` + client + `_pkey PRIMARY KEY (id);
	`
	_, err = db.Exec(sqlStatement)
	if err != nil {
		fmt.Println(err)
		db.Close()
		return
	}
	fmt.Println("Added new table")
	db.Close()
}

// Global usage random -----------
func randomWeigth() int {
	return (rand.Int()%(rand.Int()+1)*rand.Int()%(rand.Int()))%(rand.Int()+1) + 1
}

func randomEventKind() string {
	kinds := []string{"win", "lost", "play", "click"}
	return kinds[rand.Int()%len(kinds)]
}

// -------------------------------
// Fake user creation tools ------
func initOs() []string {
	osList := []string{"4.1 JellyBean", "4.4 KitKat", "5.0 Lollipop", "6.0 Marshmallow", "7.0 Nougat", "8.0 Oreo"}
	return osList
}

func initProducer() []string {
	producer := []string{"sony", "samsung", "google", "alcatel", "wicko", "lg", "nokia", "blackberry"}
	return producer
}

func randomOs() string {
	i := rand.Int() % len(os)
	return os[i]
}

func randomProducer() string {
	i := rand.Int() % len(producer)
	return producer[i]
}

func randomRes() string {
	x := rand.Int() % 1200
	y := rand.Int() % 800
	return strconv.Itoa(x) + "x" + strconv.Itoa(y)
}

func newFakePlatform() *user.Platform {
	plt := user.InitPlatform()
	plt.SetUserAgent(fake.UserAgent())
	plt.SetUniqueDeviceID(user.GetUUID())
	plt.SetResolution(randomRes())
	return plt
}

func newFakeUser() user.User {
	usr := user.InitUser()
	usr.SetIP(fake.IPv4())
	usr.SetPlatform(newFakePlatform())
	for i := 0; i < randomWeigth(); i++ {
		*users = append(*users, *usr)
	}
	return *usr
}

func getRandomUser() user.User {
	len := len(*users)
	i := rand.Int() % (len + rand.Int()%(len/10+1))
	if i < len {
		usr := *users
		return usr[i]
	}
	return newFakeUser()
}

// -------------------------------
// Fake event creation tools -----
func newFakeValue() application.Value {
	clock := clockwork.NewFakeClock()
	ts := clock.Now().UnixNano() / int64(time.Millisecond)
	return &application.ViewValue{
		PageTitle: fake.Title(),
		Refer:     fake.DomainName(),
		Timestamp: ts,
	}
}

func newFakeAction() *application.Action {
	act := application.InitAction()
	act.SetRoot(fake.DomainName() + "." + fake.DomainZone() + "/" + fake.Brand())
	act.SetDescription(fake.Paragraph())
	return act
}

func newFakeEvent() application.Application {
	app := application.InitApplication()
	app.SetAction(newFakeAction())
	for i := 0; i < randomWeigth(); i++ {
		app.SetValue(newFakeValue())
		for i := 0; i < randomWeigth(); i++ {
			*events = append(*events, *app)
		}
	}
	return *app
}

func getRandomEvent() application.Application {
	len := len(*events)
	max := (len + rand.Int()%(len/10+1)) * 6
	i := rand.Int() % max
	if i < 1*max/2 {
		eve := *events
		return eve[i%len]
	}
	if i < 5*max/6 {
		eve := *events
		event := eve[i%len]
		return event
	}
	return newFakeEvent()
}

// -------------------------------
// Fake View event ---------------
func newFakeViewEvent() PageView {
	pageURL := fake.DomainName() + "." + fake.DomainZone() + "/" + fake.Brand()
	eventID := GetUUID()
	ve := InitPageView(pageURL, SetViewEventID(eventID))
	for i := 0; i < randomWeigth(); i++ {
		ve.SetTitle(fake.Title())
		ve.SetRefer(fake.DomainName() + "." + fake.DomainZone() + "/" + fake.Brand())
		clock := clockwork.NewFakeClock()
		ve.SetTimestamp(clock.Now().UnixNano() / int64(time.Millisecond))
		for i := 0; i < randomWeigth(); i++ {
			*viewEvents = append(*viewEvents, *ve)
		}
	}
	return *ve
}

func getRandomViewEvent() PageView {
	len := len(*viewEvents)
	i := rand.Int() % (len + rand.Int()%(len/10+1))
	if i < len {
		events := *viewEvents
		return events[i]
	}
	return newFakeViewEvent()
}

// Fake 1/X event
func newFakeOverXEvent() OverX {
	gameURL := fake.DomainName() + "." + fake.DomainZone() + "/" + fake.Brand()
	eventID := GetUUID()
	eventKind := randomEventKind()
	ge := InitOverX(gameURL, eventKind, SetOXID(eventID))
	for i := 0; i < randomWeigth(); i++ {
		mod := float64(rand.Int63())
		val := math.Abs(rand.NormFloat64())
		ge.SetCartValue(math.Mod(val, mod))
		ge.SetCurrency(fake.CurrencyCode())
		for i := 0; i < randomWeigth(); i++ {
			*overXEvents = append(*overXEvents, *ge)
		}
	}
	return *ge
}

func getRandomOverXEvent() OverX {
	len := len(*overXEvents)
	i := rand.Int() % (len + rand.Int()%(len/10+1))
	if i < len {
		events := *overXEvents
		return events[i]
	}
	return newFakeOverXEvent()
}

// -------------------------------
// Fake client creation tools ----
func newFakeVenue() client.Venue {
	ven := client.InitVenue()
	ven.SetCity(fake.City())
	ven.SetCountry(fake.Country())
	ven.SetName(fake.Brand())
	ven.SetStreet(fake.StreetAddress())
	ven.SetZipcode(fake.Zip())
	return *ven
}

func newFakeClient() client.Client {
	cli := client.InitClient()
	cli.SetCompany(fake.Company())
	initDB(cli.Company)
	cli.SetCurrency(fake.CurrencyCode())
	for i := 0; i < randomWeigth(); i++ {
		ven := newFakeVenue()
		cli.SetVenue(&ven)
		for i := 0; i < randomWeigth(); i++ {
			*clients = append(*clients, *cli)
		}
	}
	return *cli
}

func getRandomClient() client.Client {
	len := len(*clients)
	i := rand.Int() % (len + rand.Int()%(len/10+1))
	if i < len {
		cli := *clients
		return cli[i]
	}
	return newFakeClient()
}

// -------------------------------
// Fake tracker payload ----------
func newFakeTrackerPayload() *TrackerPayload {
	tpl := InitTrackerPayload()
	clock := clockwork.NewFakeClock()
	ts := clock.Now().UnixNano() / int64(time.Millisecond)
	tpl.SetAppName(fake.ProductName())
	tpl.SetTimeStamp(ts)
	for i := 0; i < randomWeigth(); i++ {
		*trackerPayloads = append(*trackerPayloads, *tpl)
	}
	return tpl
}

func getRandomTrackerPayload() *TrackerPayload {
	len := len(*trackerPayloads)
	i := rand.Int() % (len + rand.Int()%(len/10+1))
	if i < len {
		trackers := *trackerPayloads
		return &trackers[i]
	}
	return newFakeTrackerPayload()
}

// -------------------------------
func generateRandom() {
	usr := getRandomUser()
	cli := getRandomClient()
	tpl := getRandomTrackerPayload()

	Set := OptionUser(&usr)
	setCli := OptionClient(&cli)
	setTpl := RequireTracker(tpl)
	Set(tracker)
	setCli(tracker)
	setTpl(tracker)

	if rand.Int()%2 == 0 {
		eve := getRandomViewEvent()
		tracker.TrackPageView(eve)
		return
	}
	eve := getRandomOverXEvent()
	tracker.TrackOverX(eve)
}

func generateRandomX(x int) {
	for i := 0; i < x; i++ {
		generateRandom()
	}
}

func generateTesterEvent() {
	generateRandom()
}

func usePlay() {
	for i := 0; i < 50000; i++ {
		generateRandom()
	}
	again := true
	var intInput int
	for again {
		fmt.Printf("What would you like to do ?\n")
		fmt.Printf("0 - Generate 1 new random event (default)lt-\n1 - Generate n new random events\n2 - Generate your own events\n")
		fmt.Scanln(&intInput)
		switch intInput {
		case 0:
			generateRandom()
		case 1:
			fmt.Printf("How much events do you wish to generate ?\n")
			fmt.Scanln(&intInput)
			fmt.Printf("Generating ")
			fmt.Print(intInput)
			fmt.Printf(" events ...\n")
			generateRandomX(intInput)
		case 2:
			generateTesterEvent()
		default:
			generateRandom()
		}
	}
}

func main() {
	fmt.Print("Demo application for client DMP Plateform.\nThis demo will generate 50000 random events then allow you to send some custom value as events\n\n")
	os = initOs()
	producer = initProducer()
	emitter = InitEmitter(RequireCollectorURI("0.0.0.0:4000"))
	trackerPayload := InitTrackerPayload()
	tracker = InitTracker(RequireEmitter(emitter), RequireTracker(trackerPayload))
	users = &[]user.User{}
	events = &[]application.Application{}
	viewEvents = &[]PageView{}
	overXEvents = &[]OverX{}
	clients = &[]client.Client{}
	trackerPayloads = &[]TrackerPayload{}

	for i := 0; i < 30; i++ {
		newFakeViewEvent()
		newFakeOverXEvent()
	}
	for i := 0; i < 10; i++ {
		newFakeUser()
		newFakeClient()
		newFakeTrackerPayload()
	}
	usePlay()
}
