defmodule Storage.Mixfile do
  use Mix.Project

  def project do
    [
      app: :storage,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:logger, :ex_aws, :hackney, :poison, :ecto, :postgrex],
      mod: {Storage, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_aws, "~> 1.0"},
      {:hackney, "~> 1.6"},
      {:sweet_xml, "~> 0.6.5"},
      {:ex_json_schema, "~> 0.5.4"},
      {:dogma, "~> 0.1", only: :dev},
      {:poison, "~> 3.1"},
      {:ecto, "~> 2.1"},
      {:postgrex, ">= 0.0.0"}
    ]
  end
end
