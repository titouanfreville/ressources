-- Put all the Models you want to init in the database here

-- Base schema for a Client table

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

-- ##################### REQUIRED TABLE (BUBBLES DEFAULT) ############################################# --
--
-- Name: bubbles; Type: TABLE; Schema: public; Owner: bubbles-admin
--

CREATE TABLE bubbles (
    id SERIAL NOT NULL,
    "trackerId" character varying(255),
    application character varying(255),
    "trackingEventTime" timestamp without time zone,
    venue character varying(255),
    "venueCity" character varying(255),
    "venueCountry" character varying(255),
    "venueStreet" character varying(255),
    "venueZipcode" character varying(255),
    "userId" character varying(255),
    "platformProperties" character varying(255),
    "platformOs" character varying(255),
    "platformOsVersion" character varying(255),
    browser character varying(255),
    "browserVersion" character varying(255),
    "userDevice" character varying(255),
    "platformResolution" character varying(255),
    "platformUID" character varying(255),
    "platformProducer" character varying(255),
    "userCountry" character varying(255),
    decription character varying(255),
    "eventId" character varying(255),
    "eventName" character varying(255),
    "eventRoot" character varying(255),
    "userPayment" double precision,
    "userCurrency" character varying(255),
    "convertedUserPayment" double precision,
    "viewEventTitle" character varying(255),
    "viewEventRefer" character varying(255),
    "eventValueTimestamp" timestamp without time zone
);


ALTER TABLE bubbles OWNER TO "bubbles-admin";

--
-- Name: bubbles bubbles_pkey; Type: CONSTRAINT; Schema: public; Owner: bubbles-admin
--

ALTER TABLE ONLY bubbles
    ADD CONSTRAINT bubbles_pkey PRIMARY KEY (id);


-- ##################### CLIENT TABLES ##################### --

-- #### CLIENT: Nestle (test)

--
-- Name: nestle; Type: TABLE; Schema: public; Owner: bubbles-admin
--

CREATE TABLE nestle (
    id SERIAL NOT NULL,
    "trackerId" character varying(255),
    application character varying(255),
    "trackingEventTime" timestamp without time zone,
    venue character varying(255),
    "venueCity" character varying(255),
    "venueCountry" character varying(255),
    "venueStreet" character varying(255),
    "venueZipcode" character varying(255),
    "userId" character varying(255),
    "platformProperties" character varying(255),
    "platformOs" character varying(255),
    "platformOsVersion" character varying(255),
    browser character varying(255),
    "browserVersion" character varying(255),
    "userDevice" character varying(255),
    "platformResolution" character varying(255),
    "platformUID" character varying(255),
    "platformProducer" character varying(255),
    "userCountry" character varying(255),
    decription character varying(255),
    "eventId" character varying(255),
    "eventName" character varying(255),
    "eventRoot" character varying(255),
    "userPayment" double precision,
    "userCurrency" character varying(255),
    "convertedUserPayment" double precision,
    "viewEventTitle" character varying(255),
    "viewEventRefer" character varying(255),
    "eventValueTimestamp" timestamp without time zone
);


ALTER TABLE nestle OWNER TO "bubbles-admin";
--
-- Name: nestle nestle_pkey; Type: CONSTRAINT; Schema: public; Owner: bubbles-admin
--

ALTER TABLE ONLY nestle
    ADD CONSTRAINT nestle_pkey PRIMARY KEY (id);

-- ### Add new clients here --
