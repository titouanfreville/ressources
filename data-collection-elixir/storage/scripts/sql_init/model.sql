-- ### Make table for your new client: replace <<CLIENT_NAME>> by client name starting with a Maj. Replace <<client_name>> by full lower case client name.

-- #### CLIENT: <<CLIENT_NAME>> --

--
-- Name: <<client_name>>; Type: TABLE; Schema: public; Owner: bubbles-admin
--

CREATE TABLE <<client_name>> (
    id SERIAL NOT NULL,
    "trackerId" character varying(255),
    application character varying(255),
    "trackingEventTime" timestamp without time zone,
    venue character varying(255),
    "venueCity" character varying(255),
    "venueCountry" character varying(255),
    "venueStreet" character varying(255),
    "venueZipcode" character varying(255),
    "userId" character varying(255),
    "platformProperties" character varying(255),
    "platformOs" character varying(255),
    "platformOsVersion" character varying(255),
    browser character varying(255),
    "browserVersion" character varying(255),
    "userDevice" character varying(255),
    "platformResolution" character varying(255),
    "platformUID" character varying(255),
    "platformProducer" character varying(255),
    "userCountry" character varying(255),
    decription character varying(255),
    "eventId" character varying(255),
    "eventName" character varying(255),
    "eventRoot" character varying(255),
    "userPayment" double precision,
    "userCurrency" character varying(255),
    "convertedUserPayment" double precision,
    "viewEventTitle" character varying(255),
    "viewEventRefer" character varying(255),
    "eventValueTimestamp" timestamp without time zone
);


ALTER TABLE <<client_name>> OWNER TO "bubbles-admin";
--
-- Name: <<client_name>> <<client_name>>_pkey; Type: CONSTRAINT; Schema: public; Owner: bubbles-admin
--

ALTER TABLE ONLY <<client_name>>
    ADD CONSTRAINT <<client_name>>_pkey PRIMARY KEY (id);
