defmodule Shardstate do
  @derive [ExAws.Dynamo.Encodable]
  defstruct [:shardName, :shardIterator]
end