defmodule Storage.Bubbles do
  @moduledoc """
  The module `Storage.Base` define a squeletton for models files.
  Each time a cient is created, we need to create an associated models.

  Replace Base by the client name.
  """
  use Ecto.Schema

  schema "bubbles" do
    # Tracker informations
    field :trackerId
    field :application
    field :trackingEventTime, :utc_datetime
    # Client informations
    field :venue
    field :venueCity
    field :venueCountry
    field :venueStreet
    field :venueZipcode
    # User informations
    field :userId
    field :platformProperties
    field :platformOs
    field :platformResolution
    field :platformUID
    field :platformProducer
    field :userCountry
    # Application informations
    # # Common part
    field :decription
    field :eventId
    field :eventName
    field :eventRoot
    # # Value part. Update with your own needs
    field :viewEventTitle
    field :viewEventRefer
    field :eventValueTimestamp, :utc_datetime
  end


  def timestamp_to_datetime timestamp do
    if timestamp == nil do
      DateTime.utc_now
    else
      date = DateTime.from_unix(timestamp)
      if elem(date, 0) != :ok do
        DateTime.utc_now
      else
        elem(date, 1)
      end
    end
  end

  def parse_object json_obj do
    tracker = %{}
    venue = %{}
    user = %{}
    platform = %{}
    client = %{}
    event = %{}
    application = %{}
    opt_value = %{}
    tracker =
      unless Map.has_key?(json_obj, "tracker") do
        tracker
      else
        json_obj["tracker"]
      end

    client =
      unless Map.has_key?(json_obj, "client") do
        client
      else
        json_obj["client"]
      end

    venue =
      unless Map.has_key?(client, "venue") do
        venue
      else
        client["venue"]
      end

    user =
      unless Map.has_key?(json_obj, "user") do
        user
      else
        json_obj["user"]
      end

    platform =
      unless Map.has_key?(user, "platform") do
        platform
      else
        user["platform"]
      end

    application =
      unless Map.has_key?(json_obj, "application") do
        application
      else
        json_obj["application"]
      end

    event =
      unless Map.has_key?(application, "action") do
        event
      else
        application["action"]
      end

    opt_value =
      unless Map.has_key?(application, "value") do
        opt_value
      else
        application["value"]
      end

    %Storage.Bubbles{
      # Tracker informations
      trackerId: tracker["id"],
      application: tracker["app-name"],
      trackingEventTime: timestamp_to_datetime(tracker["timestamp"]),
      # Client informations
      venue: venue["name"],
      venueCity: venue["city"],
      venueCountry: venue["country"],
      venueStreet: venue["street"],
      venueZipcode: venue["zipcode"],
      # User informations
      userId: user["id"],
      platformOs: platform["os"],
      platformResolution: platform["resolution"],
      platformUID: platform["producer"],
      platformProducer: platform["unique-device-id"],
      userCountry: user["country"],
      # Application informations
      # # Common part
      decription: event["description"],
      eventId: event["id"],
      eventName: event["name"],
      eventRoot: event["root"],
      # # Value part. Update with your own needs
      viewEventTitle: opt_value["title"],
      viewEventRefer: opt_value["refer"],
      eventValueTimestamp: timestamp_to_datetime(opt_value["timestamp"])
    }
  end
end
