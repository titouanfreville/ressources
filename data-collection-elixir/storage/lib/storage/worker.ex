defmodule Storage.Worker do
  @moduledoc """
  Running part of the Storage application.
  """
  use GenServer

  require Logger
  alias Storage.Helper, as: Helper

  @db_system "postgres"

  def start_link do
    GenServer.start_link(__MODULE__, [])
  end

  def init [] do
    Logger.debug "test"
    main()
    {:ok, []}
  end

  def handle_info(:work, state) do
    state = do_work(state)
    main()
    {:noreply, state}
  end

  defp do_work state do
    Logger.info("Looping")
    data = Helper.build_message_list "kinesis"
    Helper.data_to_base(data, @db_system)
    state
  end

  defp main do
    Process.send_after(self(), :work, 5_000)
  end

end
