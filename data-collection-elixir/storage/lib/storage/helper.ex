defmodule Storage.Helper do
  @moduledoc """
  `Storage.Helper` defines usefull function tu process storage.
  """
  alias Storage.Repo, as: Repo
  alias ExAws.Kinesis, as: Kinesis
  alias ExAws.Dynamo, as: Dynamo
  alias Storage.Client, as: Client

  require Logger

  @queue Application.get_env(:storage, :aws_sqs_queue)
  @stream Application.get_env(:storage, :aws_kinesis_stream)
  @shard_id Application.get_env(:storage, :kinesis_shard_id)
  @default_db_system "postgres"
  @default_src "kinesis"

  def add_object(obj, src) do
    Logger.debug src
    added = obj
    |> Ecto.put_meta(source: src)
    |> Repo.insert()
    if elem(added,0) != :ok do
      Logger.error(
        "Error while adding object into table "
        <> src <> "\n"
        <> Poison.encode!(elem(added, 1))
        )
    else
      Logger.info(
        "Correctly added object into table "
        <> src <> "\n"
      )
    end
  end

  def json_to_shard [] do
    []
  end

  def json_to_shard [t | q] do
    [t["ShardId"]] ++ json_to_shard q
  end

  defp build_shard_list(shards, false) do
    json_to_shard shards
  end

  defp build_shard_list(shards, true) do
    json_to_shard shards ++ get_kinesis_shards()
  end

  def get_kinesis_shards do
    stream_info =
      @stream
      |> Kinesis.describe_stream
      |> ExAws.request!

    stream = stream_info["StreamDescription"]
    build_shard_list(stream["Shards"], stream["HasMoreShards"])
  end

  defp records_to_data [] do
    []
  end

  defp records_to_data [t | q] do
    data =
      case Base.decode64(t["Data"]) do
        {:ok, decoded} -> case Poison.decode(decoded) do
          {:ok, data} -> data
          :error ->
            Logger.error("Could not decode data from:" <> decoded)
          end
        :error ->
          Logger.error("Could not decode data from: #{inspect t}")
      end
    [data] ++ records_to_data q
  end

  defp process_message shard, shard_iterator do
    rq =
      shard_iterator
      |> Kinesis.get_records
      |> ExAws.request

    if elem(rq, 0) != :ok do
      error = elem(elem(rq, 1),2)
      if error["__type"] == "ExpiredIteratorException" do
        Logger.debug "Too Old iterator"
        raise "simba"
      else
       Logger.error(
        "Error while getting records from Kinesis stream: "
        <> @stream <> "\nMessage: " <>
        (elem(elem(rq, 1), 2))["message"]
      )
      []
      end
    else
      records = elem(rq, 1)
      if records["Records"] == [] do
        :timer.sleep 500
        next_message shard, records
        []
      else
        Logger.debug "Got datas"
        data = records_to_data records["Records"]
        data_to_client(data, "postgres")
        data_to_base(data, "default")
        next_message shard, records
        []
      end
    end
  end

  defp next_message shard, prv_response do
    next_shard = prv_response["NextShardIterator"]
    if next_shard == nil do
      []
    else
      Logger.info "Next shard iterator"
      save_kinesis_shard_iterator shard, next_shard
      nxt_message = process_message shard, next_shard
      prv_response["Records"] ++ nxt_message
    end
  end

  defp init_shard_iterator shard, position do
    rq =
      @stream
      |> Kinesis.get_shard_iterator(shard, position)
      |> ExAws.request

    if elem(rq, 0) != :ok do
      if position == :latest do
        Logger.error(
          "Error while getting shard_iterator from Kinesis stream: "
          <> @stream <> "\nErrors: " <>
          Poison.encode!(elem(elem(rq, 1), 2))
        )
      else
        init_shard_iterator shard, :latest
      end
    else
      process_message shard, elem(rq, 1)["ShardIterator"]
    end
  end

  def get_kinesis_records do
    Logger.info "Getting records ..."
    try do
      stream = @stream
      shardFromDynamo =
        stream
        |> Dynamo.get_item(%{shardName: @shard_id})
        |> ExAws.request!
        |> Dynamo.decode_item(as: Shardstate)

      shardFromDynamo = shardFromDynamo.shardIterator
      if shardFromDynamo != nil do
        process_message @shard_id, shardFromDynamo
      else
        raise "it's the tale of life"
      end
    rescue
      _ -> init_shard_iterator @shard_id, :latest
    end
  end

  # @get_sqs_message
  # Get the last message uploaded in SQS Queue
  # @return message parsed as tupe containing his handler and his body
  # @return nil if no message or error
  def get_sqs_message do
    Logger.info "Getting SQS message"
    request =
      @queue
      |> ExAws.SQS.receive_message
      |> ExAws.request

    if elem(request, 0) != :ok do
      Logger.warn(
        "Error while getting messages from SQS"
        <> Poison.encode!(elem(request, 1)))
      nil
    else
      body = elem(request, 1)[:body]
      messages = body[:messages]
      if Enum.empty?(messages) do
        Logger.info "No more messages"
        nil
      else
        message = Enum.map(
          messages,
          fn mes ->
            {mes[:receipt_handle], Poison.decode!(mes[:body])}
          end)
        hd(message)
      end
    end
  end

  def build_sqs_message_list do
    tp = get_sqs_message()
    if tp == nil do
      []
    else
      remove_sqs_message tp
      [elem(tp, 1)] ++ build_sqs_message_list()
    end
  end

  def build_message_list "sqs" do
    build_sqs_message_list()
  end

  def build_message_list "kinesis" do
    get_kinesis_records()
  end

  def build_message_list _default do
    build_message_list @default_src
  end

  # @remove_sqs_message
  # Remove read message from SQS queue
  # @param mes tuple message with its content and its handle id
  # @ensure message deleted from SQS queue
  # @log Error if an error occur during deletion
  def remove_sqs_message mes do
    Logger.info "Removing SQS message"
    queue = @queue
    receipt_handle = elem(mes, 0)
    request =
      queue
      |> ExAws.SQS.delete_message(receipt_handle)
      |> ExAws.request

    if elem(request, 0) != :ok do
      Logger.warn(
        "Error while deleting messages from SQS"
        <> Poison.decode!(elem(request, 1)))
    else
      Logger.info "Message deleted"
    end
  end

  def parse_tracker tracker do
    new_tracker = %{}
    new_tracker =
      unless Map.has_key?(tracker, "id") do
        new_tracker
      else
        Map.put(new_tracker, :trackerId, tracker["id"])
      end
    new_tracker
  end

  def parse_obj obj do
    new_obj = %{}
    new_obj =
      unless Map.has_key?(obj, "tracker") do
        new_obj
      else
        tracker = parse_tracker obj["tracker"]
        Map.put(new_obj, "tracker", tracker)
      end
    new_obj
  end

  def data_to_base([], _db) do
    Logger.info "Data send to Database -- default tables !"
  end

  def data_to_base([t | q], "postgres") do
    obj = Client.parse_object t
    add_object(obj, "bubbles")
    data_to_base(q, "postgres")
  end

  # def data_to_base([t | q], "redshift") do
  # end

  def data_to_base(list, _param \\ nil) do
    data_to_base(list, @default_db_system)
  end

  defp to_client(client, data, "postgres") do
    obj = Client.parse_object data
    add_object(obj, client)
  end

  def data_to_client([], "postgres") do
    Logger.info "Data send to Database -- Client tables !"
  end

  def data_to_client([t | q], "postgres") do
    client = t["client"]
    if Map.has_key?(t, "client") &&
      (Map.has_key?(client, "company") || Map.has_key?(client, "id")) do
      cli = client["company"]
      to_client(String.downcase(cli), t, "postgres")
      data_to_client(q, "postgres")
    else
        data_to_base([t], "postgres")
        data_to_client(q, "postgres")
    end
  end

  # def data_to_client([t | q], "redshift") do

  # end

  def data_to_client(list, _any \\ nil) do
    data_to_client(list, @default_db_system)
  end

  def data_to_client(list, _any, db) do
    data_to_client(list, db)
  end

  def save_kinesis_shard_iterator shard, shardIterator do
    try do
      @stream
      |> Dynamo.create_table("shardName", %{shardName: :string}, 2, 2)
      |> ExAws.request!
      :timer.sleep 2000
      Logger.info "Save table created"
    rescue
      _ -> Logger.info "Save table already exists"
    end
    item = %Shardstate{shardName: shard, shardIterator: shardIterator}
    create =
      @stream
      |> Dynamo.put_item(item)
      |> ExAws.request()

    if elem(create, 0) != :ok do
      Logger.error "Error while adding shard state to dynamo"
    end
  end
end
