defmodule Storage.Repo do
  @moduledoc """
  Load Ecto.Repo for storage application
  """
  use Ecto.Repo, otp_app: :storage

end
