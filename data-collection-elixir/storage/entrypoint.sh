#!/bin/bash
DATABASE_HOST=${DATABASE_HOST:-"dmp-database"}
DB_PORT=${DATABASE_PORT:-5432}
echo "FORCE_MIGRATION before: $FORCE_MIGRATION"
FORCE_MIGRATION=${FORCE_MIGRATION:-1}
echo "FORCE_MIGRATION after: $FORCE_MIGRATION"
echo "> Getting dependencies"
mix deps.get
echo "<<<<<<<<<<<<<<<<<<<<<<"
echo "> Waiting for database"
waitforit $DATABASE_HOST:$DB_PORT -t 0
echo "> DB Ready <<<<<<<<<<<"
echo ">>> Running"
iex -S mix
