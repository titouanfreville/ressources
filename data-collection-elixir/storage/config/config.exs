# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :storage,
  aws_sqs_queue:
    System.get_env("AWS_SQS_ENRICHED_NAME")
      || "bubbles-enriched-dev",
  aws_kinesis_stream:
    System.get_env("AWS_KYNESIS_STREAM_NAME")
    || "bubbles-enriched-dev",
  default_data_schema:
    System.get_env("DEFAULT_DATA_SCHEMA")
    || "default.json",
  default_data_manager_process: # "sqs" or "kinesis"
    System.get_env("DEFAULT_DATA_MANAGER_PROCESS")
    || "kinesis",
  kinesis_shard_id:
    System.get_env("KINESIS_SHARD_ID")
    || "shardId-000000000000",
  ecto_repos: [Storage.Repo]

config :storage, Storage.Repo,
  adapter: Ecto.Adapters.Postgres,
  database:
    System.get_env("DATABASE_NAME")
    || "bubbles-storage",
  username:
    System.get_env("DATABASE_USERNAME")
    || "bubbles-admin",
  password:
    System.get_env("DATABASE_PASSWORD")
    || "t6vwnj",
  hostname:
    System.get_env("DATABASE_HOST")
    || "dmp-database"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure AWS
config :ex_aws,
  region: [{:system, "AWS_REGION"}, :instance_role],
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
  secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role]

config :ex_aws, :retries,
  max_attempts: [{:system, "AWS_SQS_MAX_ATTEMPTS"}, :instance_role]
