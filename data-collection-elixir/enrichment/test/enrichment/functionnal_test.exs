defmodule TestEnrichment do
  @moduledoc false

  use ExUnit.Case, async: false

  import ExUnit.CaptureLog
  alias Enrichment.Helper
  require Logger

  @moduletag :capture_log
  @raw_stream Application.get_env(:enrichment, :aws_raw_stream)
  @enriched_stream Application.get_env(:enrichment, :aws_enriched_stream)

  test "#send_message/1,2 should fail if kinesis errored" do
    remove_streams()

    assert Helper.send_message(%{
      "test" => "test"
    }, "kinesis") == :ok

    res_logs = capture_log(
      fn ->
        Helper.send_message(%{
          "test" => "test"
        }, "kinesis")
      end
    )

    assert String.contains?(res_logs, "Error")
    refute String.contains?(res_logs, "Message sent")
  end

  test "#send_message/1,2 have to send correct datas" do
    init_streams()

    assert Helper.send_message(%{
      "test" => "test"
    }, "kinesis") == :ok

    res_logs = capture_log(
      fn ->
        Helper.send_message(%{
          "test" => "test"
        }, "kinesis")
      end
    )

    refute String.contains?(res_logs, "Error")

    assert String.contains?(capture_log(
      fn ->
        Helper.send_message(%{
          "test" => "test"
        }, "kinesis")
      end
    ), "Message sent")

    assert Helper.send_message(%{
      "test" => "test"
    }) == :ok

    refute String.contains?(capture_log(
      fn ->
        Helper.send_message(%{
          "test" => "test"
        })
      end
    ), "Error")

    assert String.contains?(capture_log(
      fn ->
        Helper.send_message(%{
          "test" => "test"
        })
      end
    ), "Message sent")
  end

  test "#save_kinesis_shard_iterator/2 should correctly add the datas" do
    try do
      @raw_stream
      |> ExAws.Kinesis.create_stream(1)
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end

    try do
      @enriched_stream
      |> ExAws.Kinesis.create_stream(1)
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end

    try do
      @raw_stream
      |> ExAws.Dynamo.delete_table()
      |> ExAws.request!

      :timer.sleep 2000
    rescue
      _ -> Logger.debug "Ok"
    end

    refute String.contains?(capture_log(
      fn ->
        Helper.save_kinesis_shard_iterator("test", "test")
      end
    ), "Error")

    refute String.contains?(capture_log(
      fn ->
        Helper.save_kinesis_shard_iterator("test", "test")
      end
    ), "error")

    refute String.contains?(capture_log(
      fn ->
        Helper.save_kinesis_shard_iterator("test", "test")
      end
    ), "Error")
  end

  # test "get_kinesis_records should behave correctly" do

  #   res = capture_log(fn ->
  #     Helper.start_enrich()
  #   end)

  #   assert String.contains?(res, "Getting records ...")
  #   refute String.contains?(res, "error")
  #   refute String.contains?(res, "Too Old")

  # end

  # ------ HELPER FUNCTIONS -------------------

  defp init_streams do
    try do
      @raw_stream
      |> ExAws.Kinesis.create_stream(1)
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end

    try do
      @enriched_stream
      |> ExAws.Kinesis.create_stream(1)
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end
  end

  defp remove_streams do
    # Create stream
    try do
      @raw_stream
      |> ExAws.Kinesis.delete_stream
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end

    try do
      @enriched_stream
      |> ExAws.Kinesis.delete_stream
      |> ExAws.request!
      :timer.sleep 2000
    rescue
      _ -> Logger.error "stream exist"
    end
  end

  defp remove_table do
    try do
      @raw_stream
      |> ExAws.Dynamo.delete_table()
      |> ExAws.request!

      :timer.sleep 2000
    rescue
      _ -> Logger.debug "Ok"
    end
  end

end
