defmodule TestMoneyFreeRatesGetter do
  @moduledoc false

  use ExUnit.Case, async: true
  alias Money.ExchangeRates.MyExchangeRates, as: TestModule
  # @moduletag :capture_log

  test "get_latest_rates/0 should return a non empty list" do
    map = TestModule.get_latest_rates()
    assert elem(map, 0) == :ok
    refute elem(map, 1) == []
  end

  test "get_rate/1 should return error if unexpected things happend" do
    res = TestModule.get_rate("http://this.is.not.url.fr/")
    assert elem(res, 0) == :error
  end
end
