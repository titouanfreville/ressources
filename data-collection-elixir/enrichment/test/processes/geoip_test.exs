defmodule TestEnrichmentGeoip do
  @moduledoc false
  alias Enrichment.Enrich.Geoip
  use ExUnit.Case, async: true
  @moduletag :capture_log

  defp string_list?([]) do
    true
  end

  defp string_list?([t | q]) do
    is_bitstring(t) && string_list?(q)
  end

  test "load/0 must return []" do
    assert Geoip.load == []
  end

  test "accepted_name/0 must return a list of string" do
    list = Geoip.accepted_names

    assert string_list?(list)
  end

  test "run/1 must return enrich correct message" do
    message = %{
      "user" => %{
        "ip" => "8.8.8.8"
      }
    }

    res = Geoip.run message

    refute Map.equal?(res, message)
  end

  test "run/1 should not enrich the message if not
  correct" do
    message = %{}

    res = Geoip.run message

    assert Map.equal?(res, message)

    message = %{
      "test" => "test"
    }

    res = Geoip.run message

    assert Map.equal?(res, message)

    message = %{
      "user" => %{
        "test" => "test"
      }
    }

    res = Geoip.run message

    assert Map.equal?(res, message)
  end
end
