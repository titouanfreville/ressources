defmodule TestEnrichmentAgentParsing do
  @moduledoc false
  alias Enrichment.Enrich.Useragent
  use ExUnit.Case, async: true
  @moduletag :capture_log

  defp string_list?([]) do
    true
  end

  defp string_list?([t | q]) do
    is_bitstring(t) && string_list?(q)
  end

  test "load/0 must return []" do
    assert Useragent.load == []
  end

  test "accepted_name/0 must return a list of string" do
    list = Useragent.accepted_names

    assert string_list?(list)
  end

  test "run/1 should enrich message" do
    # Full UA
    ua =
      "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; "
      <> "IEMobile/10.0; ARM; Touch; Microsoft; Lumia 640 XL)"

    message = %{
      "user" => %{
        "platform" => %{
          "user-agent" => ua
        }
      }
    }

    res1 = Useragent.run(message)

    refute Map.equal?(message, res1)

    info1 = res1["user"]["platform"]

    assert Map.has_key?(info1, "os")
    assert Map.has_key?(info1, "os-version")
    assert Map.has_key?(info1, "browser")
    assert Map.has_key?(info1, "browser-version")
    assert Map.has_key?(info1, "producer")
    assert Map.has_key?(info1, "device")
    # ----
    # No OS UA
    ua1 =
      "Mozilla/5.0 (compatible; MSIE 10.0; Trident/6.0; "
      <> "IEMobile/10.0; ARM; Touch; Microsoft; Lumia 640 XL)"
    message2 = %{
      "user" => %{
        "platform" => %{
          "user-agent" =>  ua1
        }
      }
    }
    res2 = Useragent.run(message2)

    info2 = res2["user"]["platform"]

    refute Map.has_key?(info2, "os")
    refute Map.has_key?(info2, "os-version")
    assert Map.has_key?(info2, "browser")
    assert Map.has_key?(info2, "browser-version")
    assert Map.has_key?(info2, "producer")
    assert Map.has_key?(info2, "device")
    # ----
    # No Browser UA
    ua2 =
    "compatible; Windows Phone 8.0; Trident/6.0; "
    <> "ARM; Touch; Microsoft; Lumia 640 XL"

    message3 = %{
      "user" => %{
        "platform" => %{
          "user-agent" =>  ua2
        }
      }
    }
    res3 = Useragent.run(message3)

    info3 = res3["user"]["platform"]

    refute Map.has_key?(info3, "os")
    refute Map.has_key?(info3, "os-version")
    refute Map.has_key?(info3, "browser")
    refute Map.has_key?(info3, "browser-version")
    assert Map.has_key?(info3, "producer")
    assert Map.has_key?(info3, "device")
    # ----
    # No OS/DEVICE UA
    ua3 =
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; "
    <> "IEMobile/10.0)"

    message4 = %{
      "user" => %{
        "platform" => %{
          "user-agent" =>  ua3
        }
      }
    }
    res4 = Useragent.run(message4)

    info4 = res4["user"]["platform"]

    assert Map.has_key?(info4, "os")
    assert Map.has_key?(info4, "os-version")
    assert Map.has_key?(info4, "browser")
    assert Map.has_key?(info4, "browser-version")
    assert Map.has_key?(info4, "producer")
    assert info4["producer"] == "Generic"
    assert Map.has_key?(info4, "device")
    assert info4["device"] == "Smartphone"
    # ---
  end

  test "run/0 return initial message when no UA existes" do
    message = %{}
    res = Useragent.run(message)
    assert res == message

    message = %{"user" => %{}}
    res = Useragent.run(message)
    assert res == message

    message = %{"user" => %{"platform" => %{"id" => "test"}}}
    res = Useragent.run(message)
    assert res == message


    # Empty UA
    message = %{
      "user" => %{
        "platform" => %{
          "user-agent" =>  ""
        }
      }
    }
    res = Useragent.run(message)

    info = res["user"]["platform"]

    refute Map.has_key?(info, "os")
    refute Map.has_key?(info, "os-version")
    refute Map.has_key?(info, "browser")
    refute Map.has_key?(info, "browser-version")
    refute Map.has_key?(info, "producer")
    refute Map.has_key?(info, "device")
    # ---
  end
end
