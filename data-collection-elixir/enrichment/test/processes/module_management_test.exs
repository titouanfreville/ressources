defmodule TestEnrichmentModuleManagement do
  @moduledoc false
  alias Enrichment.Enrich, as: Loader
  use ExUnit.Case, async: true

  @moduletag :capture_log


  defp string_list?([]) do
    true
  end

  defp string_list?([t | q]) do
    is_bitstring(t) && string_list?(q)
  end

  test "valid_name/0 should return a string list" do
    res = Loader.valid_names()
    assert string_list? res
  end
end
