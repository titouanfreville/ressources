defmodule TestEnrichmentEnrich do
  @moduledoc false
  use ExUnit.Case, async: true
  alias Enrichment.Loader
  @moduletag :capture_log

  test "init_enrich/1 should return message if 
  no enrich is defined or unknow enrich" do
    message = %{"test" => "test"}
    Application.put_env(:enrichment, :enrichment_functions, "")
    res = Loader.init_enrich message

    assert Map.equal?(res, message)

    Application.put_env(:enrichment, :enrichment_functions, "unknow")
    res = Loader.init_enrich message

    assert Map.equal?(res, message)
  end

  test "init_enrich/1 should enrich message if known enrich are defined" do
    ua =
      "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; "
      <> "IEMobile/10.0; ARM; Touch; Microsoft; Lumia 640 XL)"

    message = %{
      "user" => %{
        "platform" => %{
          "user-agent" => ua
        }
      }
    }

    Application.put_env(:enrichment, :enrichment_functions, "ua")

    res = Loader.init_enrich message

    refute Map.equal?(message, res)

    info = res["user"]["platform"]

    assert Map.has_key?(info, "os")
    assert Map.has_key?(info, "os-version")
    assert Map.has_key?(info, "browser")
    assert Map.has_key?(info, "browser-version")
    assert Map.has_key?(info, "producer")
    assert Map.has_key?(info, "device")
  end
end
