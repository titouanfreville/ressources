defmodule TestEnrichmentMoney do
  @moduledoc false
  alias Enrichment.Enrich.Money
  use ExUnit.Case, async: true
  @moduletag :capture_log

  defp string_list?([]) do
    true
  end

  defp string_list?([t | q]) do
    is_bitstring(t) && string_list?(q)
  end

  test "load/0 must return []" do
    assert Money.load == []
  end

  test "accepted_name/0 must return a list of string" do
    list = Money.accepted_names

    assert string_list?(list)
  end

  test "run/0 should enrich correct message" do
    message = %{
      "application" => %{
        "value" => %{
          "currency" => "EUR",
          "cart-value" => 10
        }
      },
      "client" => %{
        "official-currency" => "EUR"
      }
    }

    res = Money.run(message)
    value = res["application"]["value"]

    refute Map.equal?(message, res)
    assert value["converted-payment"] == value["user-payment"]
    assert value["converted-payment"] == 10

    message = %{
      "application" => %{
        "value" => %{
          "currency" => "EUR",
          "cart-value" => 10
        }
      }
    }

    res = Money.run(message)
    value = res["application"]["value"]

    refute Map.equal?(message, res)
    assert value["converted-payment"] > value["user-payment"]
    assert value["user-payment"] == 10

    message = %{
      "application" => %{
        "value" => %{
          "currency" => "EUR",
          "cart-value" => 10
        }
      },
      "client" => %{
        "test" => "test"
      }
    }

    res = Money.run(message)
    value = res["application"]["value"]
    refute Map.equal?(message, res)
    assert value["converted-payment"] > value["user-payment"]
    assert value["user-payment"] == 10

  end

  test "run/0 should not enrich incorrect message" do
    message = %{}
    res = Money.run(message)
    assert Map.equal?(res, message)
    message = %{
      "application" => %{
        "test" => "test"
      }
    }
    res = Money.run(message)
    assert Map.equal?(res, message)

    message = %{
      "application" => %{
        "value" => %{
          "test" => "test"
        }
      }
    }
    res = Money.run(message)
    assert Map.equal?(res, message)

    message = %{
      "application" => %{
        "value" => %{
          "currency" => "RND",
          "cart-value" => 10
        }
      },
      "client" => %{
        "test" => "test"
      }
    }

    res = Money.run(message)
    assert Map.equal?(res, message)

    Application.put_env(:enrichment, :default_company_currency, "RND")

    message = %{
      "application" => %{
        "value" => %{
          "currency" => "EUR",
          "cart-value" => 10
        }
      }
    }

    res = Money.run(message)
    assert Map.equal?(message, res)
  end

end
