# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :enrichment,
  aws_raw_stream:
    System.get_env("AWS_KINESIS_RAW_STREAM")
    || "bubbles-raw-dev",
  aws_enriched_stream:
    System.get_env("AWS_KYNESIS_ENRICHED_STREAM")
    || "bubbles-enriched-dev",
  shard_id:
    System.get_env("SHARD_ID")
    || "shardId-000000000000",
  ok_wait:
    System.get_env("OK_WAIT_TIME")
    || 2000, # Waiting time between iteration in ms
  wrong_wait:
    System.get_env("WRONG_WAIT_TIME")
    || 5000, # Waiting time between iteration in ms when AWS fails
  user_enrich:
    System.get_env("USER_ENRICH") # boolean value
    || false,
  enrichment_functions:
    System.get_env("ENRICHMENT_FUNCTIONS")
    || "geoip, ua, mc",
  default_company_currency:
    System.get_env("DEFAULT_COMPANY_CURRENCY")
    || "USD",
  server: true

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure AWS
config :ex_aws,
  region: [{:system, "AWS_REGION"}, :instance_role],
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
  secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role]

config :ex_aws, :retries,
  max_attempts: [{:system, "AWS_MAX_ATTEMPTS"}, :instance_role]

config :geolix,
  databases: [
    %{
      id:      :city,
      adapter: Geolix.Adapter.MMDB2,
      source:  { :system,
      "PATH_TO_CITY_MMBE_DB", "geoip/geolite/GeoLite2-City.mmdb" }
    },
    %{
      id:      :country,
      adapter: Geolix.Adapter.MMDB2,
      source:  { :system,
      "PATH_TO_CITY_MMBE_DB", "geoip/geolite/GeoLite2-Country.mmdb" }
    }
  ]

config :ex_money,
  exchange_rate_service: false,
  exchange_rates_retrieve_every: 300_000,
  delay_before_first_retrieval: 100,
  api_module: Money.ExchangeRates.MyExchangeRates,
  callback_module: Money.ExchangeRates.Callback,
  log_failure: :warn,
  log_info: :info,
  log_success: :info

import_config "#{Mix.env}.exs"
