defmodule Enrichment.Mixfile do
  use Mix.Project

  def project do
    [
      app: :enrichment,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env),
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      compile: Mix.compilers,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: cover()
    ]
  end

  def cover do
    [
      "coveralls": :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test
    ]
  end

  def application do
    [
      mod: {Enrichment, []},
      extra_applications: [
        :logger, :ex_aws, :hackney,
        :poison, :geolix, :user_agent_parser, :ex_money]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test) do
     ["lib", "test/enrichment", "test/money", "test/processes"]
  end

  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:ex_aws, "~> 1.0"},
      {:hackney, "~> 1.6"},
      {:sweet_xml, "~> 0.6.5"},
      {:ex_json_schema, "~> 0.5.4"},
      {:dogma, "~> 0.1", only: [:dev, :test]},
      {:poison, "~> 3.1"},
      {:geolix, "~> 0.14"},
      {:user_agent_parser, "~> 1.0"},
      {:ex_money, "~> 0.5.0"},
      {:bypass, "~> 0.7", only: :test},
      {:excoveralls, "~> 0.7", only: :test}
    ]
  end
end
