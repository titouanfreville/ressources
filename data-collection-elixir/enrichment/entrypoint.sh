echo ">> Getting deps"
mix deps.get
echo ">> done"
if [[ -n "$CUSTOM_ENRICH_FILE_PATH" ]]
then
    cp "$CUSTOM_ENRICH_FILE_PATH" "$PROJECT_HOME/lib/enrichment/user-enrich.ex"
fi

iex -S mix