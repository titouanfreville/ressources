defmodule Enrichment.Enrich.Useragent do
  @moduledoc """
  Parse user agent to add useful data
  """

  def load, do: []

  def accepted_names, do: ["ua", "ua_parser"]

  defp add_in_message(message, key, val) do
    Map.put(
      message, "user",
      Map.put(message["user"], "platform",
        Map.put(message["user"]["platform"], key, val)
      )
    )
  end

  defp get_ua message do
    ua_map = message["user"]["platform"]
    unless (
      Map.has_key?(message, "user") &&
      Map.has_key?(message["user"], "platform") &&
      Map.has_key?(ua_map, "user-agent")
    ) do
      message
    else
      agent = ua_map["user-agent"]
      browser = UserAgentParser.detect_browser(agent)
      os = UserAgentParser.detect_os(agent)
      device = UserAgentParser.detect_device(agent)
      nm =
        try do
          nm = add_in_message message, "os", os.family
          nm = add_in_message nm, "os-version", os.version
          nm
        rescue
          _ -> message
        end
      nm =
        try do
          nm = add_in_message nm, "browser", browser.family
          nm = add_in_message nm, "browser-version", browser.version
          nm
        rescue
          _ -> message
        end
      nm =
        try do
          nm = add_in_message nm, "producer", device.brand
          nm = add_in_message nm, "device", device.model
          nm
        rescue
          _ -> message
        end
        nm
    end
  end

  def run message do
    get_ua message
  end
end
