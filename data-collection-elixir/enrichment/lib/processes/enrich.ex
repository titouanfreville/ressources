defmodule Enrichment.Loader do
  @moduledoc """
  Load enrichment processes from configuration
  """
  alias Enrichment.Enrich, as: Loader

  def init_enrich message do
    :enrichment
    |> Application.get_env(:enrichment_functions)
    |> String.split(", ", trim: true)
    |> process_enrich(message)
  end

  defp process_enrich [], message do
    message
  end

  defp process_enrich [t | q], message do
    Loader.dynamic_dispatch t, (process_enrich q, message)
  end
end
