defmodule Enrichment.Enrich do
  @moduledoc """
  Enrich process manager
  """
  @callback accepted_names() :: List.t
  @callback run :: Anything

  # Not used
  def valid_names do
    relevant_sub_modules()
    |> Enum.map(fn(mod) -> mod.accepted_names end)
    |> List.flatten
  end

  def dynamic_dispatch(name, message) do
    relevant_sub_modules()
    |> Enum.find(fn(mod) -> name?(mod.accepted_names, name) end)
    |> call_for_module(message)
  end

  defp relevant_sub_modules do
    :code.all_loaded
    |> Enum.map(& elem(&1, 0))
    |> Enum.filter(& String.starts_with?(Atom.to_string(&1), "#{__MODULE__}."))
  end

  defp call_for_module(module, message) do
    if module == nil do
      message
    else
      module.run(message)
    end
  end

  defp name?(accepted_names, name) do
    Enum.find(accepted_names, fn(x) -> x == name end)
  end
end
