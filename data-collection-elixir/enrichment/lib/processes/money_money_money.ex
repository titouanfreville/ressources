defmodule Enrichment.Enrich.Money do
  @moduledoc """
  Enrichment process to manage money operation
  like convertions
  """
  require Logger

  def load, do: []

  def accepted_names, do: ["mc", "money_converter", "convert", "banker"]

  defp add_in_message(message, key, val) do
    Map.put(
      message, "application",
      Map.put(message["application"], "value",
        Map.put(message["application"]["value"], key, val)
      )
    )
  end

  defp convert_money message do
    money_map = message["application"]["value"]
    unless (
      Map.has_key?(message, "application") &&
      Map.has_key?(message["application"], "value") &&
      Map.has_key?(money_map, "currency")) do
      message
    else
      client_official_money =
        unless (
          Map.has_key?(message, "client") &&
          Map.has_key?(message["client"], "official-currency")
        ) do
          Application.get_env(:enrichment,
          :default_company_currency)
        else
          message["client"]["official-currency"]
        end
      user_payment_money = money_map["currency"]
      val = money_map["cart-value"]
      try do
        user_money = Money.new(user_payment_money, val)
        new_money =
          Money.to_currency(user_money,
            String.to_atom(client_official_money)
          )

        nm = add_in_message message, "user-payment", val
        nm = add_in_message(
          nm, "converted-payment",
          Decimal.to_float(elem(new_money, 1).amount))
        nm
      rescue
        _ ->
          Logger.error "[rescue] Could not convert cart"
          message
      end
    end
  end

  def run(message), do: convert_money(message)
end
