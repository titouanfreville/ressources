defmodule Enrichment.Enrich.Geoip do
  @moduledoc """
  Enrichment process to get geographic localisation from IP
  """
  alias Geolix, as: Geo

  def load, do: []

  def accepted_names, do: ["geoip", "geo", "iploc", "ipcity"]

  defp get_geoip message do
    geoip_map = message["user"]
    unless (
      Map.has_key?(message, "user") &&
      Map.has_key?(geoip_map, "ip")) do
      message
    else
      localisation =
        Geo.lookup(
          geoip_map["ip"],
          [as: :raw , where: :country]
        )

      Map.put(message, "user", Map.put(message["user"],
      "country", localisation[:country][:iso_code]))
    end
  end

  def run(message) do
    get_geoip message
  end
end
