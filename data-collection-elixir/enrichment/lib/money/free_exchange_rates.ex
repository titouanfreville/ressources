defmodule Money.ExchangeRates.MyExchangeRates do
  require Logger

  @moduledoc """
  Implement function to get exhange rates from free FIXER.IO source
  """

  @behaviour Money.ExchangeRates

  @doc """
  Retrieves the latest exchange rates from Open Exchange Rates site.

  * `app_id` is a valid Open Exchange Rates app_id.  Defaults to the
  configured `app_id` in `config.exs`

  Returns:

  * `{:ok, rates}` if the rates can be retrieved

  * `{:error, reason}` if rates cannot be retrieved

  Typically this function is called by the exchange rates retrieval
  service althouhg it can be called outside that context as
  required.
  """

  @open_exchange_rate_url "http://api.fixer.io/latest?base=USD"

  @spec get_latest_rates :: {:ok, Map.t} | {:error, String.t}

  def get_latest_rates do
    url    = Money.get_env(:open_exchange_rates_url, @open_exchange_rate_url)

    get_rate(url)
  end

  def get_rate(url) do
    case :httpc.request(String.to_charlist(url)) do
      {:ok, {{_version, 200, 'OK'}, _headers, body}} ->
        %{"base" => base, "rates" => rates} = Poison.decode!(body)

        rates = Map.put(rates, base, 1.0)

        decimal_rates = rates
        |> Cldr.Map.atomize_keys
        |> Enum.map(fn {k, v} -> {k, Decimal.new(v)} end)

        {:ok, decimal_rates}

      # {_, {{_version, code, message}, _headers, _body}} ->
      #   {:error, "#{code} #{message}"}
      # not relevant with fixer API uncomment if version requirement
      # exist

      {:error, {:failed_connect, [{_, {_host, _port}}, {_, _, sys_message}]}} ->
        {:error, sys_message}
    end
  end
end
