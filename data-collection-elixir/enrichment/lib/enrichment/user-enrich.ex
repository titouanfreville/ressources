defmodule Enrichment.Custom do
  # This file is the minimal requierment for Custom enrichment process.
  # Please make sure that
  @moduledoc """
  The module `Enrichment.Custom` define user enrichment
  functions used in bubbles DMP.

  It as to contain a function `custom_enrich/1` with arrity 1 taking a list of
  JSON maps and sending back the enriched list of message.

  The `custom_enrich/1` function is the only one which will be
  called by the basic DMP enrichment process
  """
  require Logger

  @doc """
  The `custom_enrich/1` function is the interface between the user custom
  enrichment process and the basic process.
  It has to run every enrichment wanted by the user
  and return the new list of enriched messages.
  """
  def custom_enrich [] do
    Logger.info "Custom enrichment Completed"
    []
  end

  def custom_enrich [t | q] do
    nm =
      Map.put(
        t, "tracker",
        Map.put(t["tracker"], "test", "enriched-customed")
      )

    [nm] ++ custom_enrich q
  end

end
