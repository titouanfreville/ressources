defmodule Shardstate do
  @moduledoc false
  @derive [ExAws.Dynamo.Encodable]
  defstruct [:shardName, :shardIterator]
end
