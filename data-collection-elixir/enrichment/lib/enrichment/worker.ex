defmodule Enrichment.Worker do
  # use Application, opt_app: :enrichment
  @moduledoc """
  Working process for bubbles data management platform enrichment
  """
  use GenServer
  require Logger
  alias Enrichment.Helper, as: Helper

  # @proceed
  # Runner function
  # @param key list list of the passed parameters
  # @ensure process goes correctly according to passed arguments
  @doc false
  defp proceed do
    init()
  end

  # @init
  # Get all messages in SQS as list
  # @ensure got all messages from SQS as list and removed them
  @doc false
  defp init do
    Logger.info "Started init process"
    Enrichment.Enrich.Geoip.load()
    Enrichment.Enrich.Useragent.load()
    Enrichment.Enrich.Money.load()
    Helper.start_enrich()
  end


  def start_link do
    GenServer.start_link(__MODULE__, [])
  end

  def init [] do
    main()
    {:ok, []}
  end

  def handle_info(:work, state) do
    do_work()
    main()
    {:noreply, state}
  end

  defp do_work do
    Logger.info("Looping")
    proceed()
  end

  defp main do
    Process.send_after(self(), :work, 500)
  end
end
