 defmodule Enrichment.Helper do
  require Logger
  @moduledoc """
  Implement helpers for enrichment process
  """
  alias ExAws.Kinesis, as: Kinesis
  alias ExAws.Dynamo, as: Dynamo

  @raw_stream Application.get_env(:enrichment, :aws_raw_stream)
  @enriched_stream Application.get_env(:enrichment, :aws_enriched_stream)
  @shard_id Application.get_env(:enrichment, :shard_id)
  @use_user_enrich Application.get_env(:enrichment, :user_enrich)

  defp enrich data do
    unless @use_user_enrich do
      Enrichment.Loader.init_enrich data
    else
      data
      |> Enrichment.Loader.init_enrich
      |> Enrichment.Custom.custom_enrich
    end
  end

  defp enrich_data [] do
    Logger.info "Enrichment complete"
  end

  defp enrich_data [t | q] do
    e = enrich t
    send_message e
    enrich_data q
  end

  defp records_to_data [] do
    []
  end

  defp records_to_data [t | q] do
    data =
      case Base.decode64(t["Data"]) do
        {:ok, decoded} -> case Poison.decode(decoded) do
          {:ok, data} -> data
          :error ->
            Logger.error("Could not decode data from:" <> decoded)
          end
        :error ->
          Logger.error("Could not decode data from: #{inspect t}")
      end
    [data] ++ records_to_data q
  end

  defp process_message(shard, shard_iterator) do
    Logger.debug shard_iterator
    rq =
      shard_iterator
      |> Kinesis.get_records
      |> ExAws.request

    if elem(rq, 0) != :ok do
      error = elem(elem(rq, 1),2)
      if error["__type"] == "ExpiredIteratorException" do
        Logger.debug "Too Old iterator"
        raise "simba"
      else
       Logger.error(
        "Error while getting records from Kinesis stream: "
        <> @raw_stream <> "\nMessage: "
        <> (elem(elem(rq, 1), 2))["message"]
      )
      []
      end
    else
      records = elem(rq, 1)
      if records["Records"] == [] do
        :timer.sleep 500
        next_message shard, records
        []
      else
        Logger.debug "Got datas"
        datas = records_to_data records["Records"]
        enrich_data datas
        next_message shard, records
      end
    end
  end

  defp next_message(shard, prv_response) do
    next_shard = prv_response["NextShardIterator"]
    if next_shard == nil do
      []
    else
      Logger.info "Next shard iterator"
      save_kinesis_shard_iterator shard, next_shard
      process_message shard, next_shard
    end
  end

  defp init_shard_iterator(position) do
    rq =
      @raw_stream
      |> Kinesis.get_shard_iterator(@shard_id, position)
      |> ExAws.request

    if elem(rq, 0) != :ok do
      if position == :latest do
        Logger.error(
          "Error while getting shard_iterator from Kinesis stream: "
          <> @raw_stream <> "\nErrors: "
          <> Poison.encode!(elem(elem(rq, 1), 2))
        )
      else
        init_shard_iterator :latest
      end
    else
      process_message @shard_id, elem(rq, 1)["ShardIterator"]
    end
  end

  def get_kinesis_records do
    Logger.info "Getting records ..."
    try do
      stream = @raw_stream
      shard_from_dynamo =
        stream
        |> Dynamo.get_item(%{shardName: @shard_id})
        |> ExAws.request!
        |> Dynamo.decode_item(as: Shardstate)

      shard_from_dynamo = shard_from_dynamo.shardIterator
      if shard_from_dynamo != nil do
        process_message @shard_id, shard_from_dynamo
      else
        raise "it's the tale of life"
      end
    rescue
      _ -> init_shard_iterator :latest
    end
  end

  def start_enrich do
    get_kinesis_records()
  end

  @doc """
  @`send_message/2`

  Send message to enriched Stream/Queue

  @param mes `map` map representing the enriched message
  @param process `string` string to choose between kinesis and kinesis process
  @ensure message send to enriched Stream/Queue
  """
  def send_message(mess, "kinesis") do
    Logger.info "Sending enriched message to KINESIS"
    message = Poison.encode(mess)
    if elem(message, 0) != :ok do
      Logger.warn "Message is not a JSON"
    else
      enriched = @enriched_stream
      request =
        enriched
        |> ExAws.Kinesis.put_record(@shard_id, elem(message, 1))
        |> ExAws.request

      if elem(request, 0) != :ok do
        Logger.warn(
          "Error while sending message to Kinesis stream: "
          <> enriched <> "\nErrors: " <>
          Poison.encode!(elem(elem(request, 1),2))
        )
      else
        Logger.info(
          "Message sent: "
          <> elem(message, 1)
        )
      end
    end
  end

  @doc """
  @`send_message/1`

  Send message to enriched Stream/Queue using default process

  @param mes `map` map representing the enriched message
  @ensure message send to enriched Stream/Queue
  """
  def send_message(mess) do
    send_message(mess, "kinesis")
  end

  def save_kinesis_shard_iterator(shard, shard_iterator) do
    try do
      @raw_stream
      |> Dynamo.create_table("shardName", %{shardName: :string}, 2, 2)
      |> ExAws.request!
      :timer.sleep 2000
      Logger.info "Save table created"
    rescue
      _ -> Logger.info "Save table already exists"
    end
    item = %Shardstate{shardName: shard, shardIterator: shard_iterator}
    create =
      @raw_stream
      |> Dynamo.put_item(item)
      |> ExAws.request()

    if elem(create, 0) != :ok do
      Logger.error "Error while adding shard state to dynamo"
    end
  end
end
