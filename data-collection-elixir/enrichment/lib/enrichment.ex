defmodule Enrichment do
  @moduledoc """
  """
  use Application

  def start(_types, _args) do
    import Supervisor.Spec, warn: false
    # List all child processes to be supervised
    children = [
      worker(Enrichment.Worker, []),
      worker(UA.Parser, []),
      supervisor(Money.ExchangeRates.Supervisor, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Enrichment.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
