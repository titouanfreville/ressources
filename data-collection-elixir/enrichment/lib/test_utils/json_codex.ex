defmodule Test.JSONCodec do
  @moduledoc """
  Make JSONCodec behave with Poison for tests
  """
  @behaviour ExAws.JSON.Codec

  defdelegate encode!(data), to: Poison
  defdelegate encode(data), to: Poison
  defdelegate decode!(data), to: Poison
  defdelegate decode(data), to: Poison
end
