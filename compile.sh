#!/bin/bash
#################################################################################
#################################################################################
############################## AVAST IPS COMPILE ################################
#################################################################################
## This script is used to compile Avast Ips project from NexwayGroup.          ##
#################################################################################
## Author: FREVILLE Titouan <tfreville@nexway.com>                             ##                                                                            
#################################################################################
#################################################################################
# Local Variables --------------------------------------------------------------
# ### COLORS ### #
green="\\033[1;32m"
red="\\033[1;31m"
basic="\\033[0;39m"
blue="\\033[1;34m"
# ### ### #
# ### PROCESS VALUES  ### #
DEFAULT="dev.hxml testing.hxml prep.hxml prod.hxml"
DOCKER_NAME_SPACE=registry.nexway.build
HAXE_COMPILER_TAG=1.1
quiet=1
interactive=0
watched=1
# Get options passed
TEMP=`getopt -o hqyw --long help,quiet,non-interactive,watch -n 'AvastIps Haxe compilation' -- "$@"`
# Help message to print for -h option (or when not providing correctly)
HELP_MESSAGE="Usage: ./compile.sh [OPTIONS] [FILES]

Compile your haxe project :)

Options:
  -h, --help              Print this help.
  -q, --quiet             Silencing scripts. Render testing and getting resources non interactive by default.
  -y, --non-interactive   Run process non interactively.
  -w, --watch             Run the process with a watcher. The process will look into haxe repository wating for changes.

Files:
  hxml file to compile. Default is dev.hxml testing.hxml prep.hxml prod.hxml
"
# ### ### #
# ------------------------------------------------------------------------------
# Function declaration ---------------------------------------------------------
# ### Wipthails input box ### #
init_inputbox()
{
  if [ $# -lt 3 ]
    then
      echo -n "Illegal number of parameters, you broke me :'( "
      echo "-- usage: init_select BACKTITLE TITLE QUESTION (HEIGHT) (WIDTH) (DEFAULT_TEXT)"
      return 1
  fi
  eval `resize`
  TEXT=""
  if [ $# -ge 4 ]
    then LINES=$4
  fi
  if [ $# -ge 5 ]
    then COLUMNS=$5
  fi
  if [ $# -ge 6 ]
    then TEXT=$6
  fi
  BACKTITLE="$1"
  TITLE="$2"
  MENU="$3"
  whiptail --clear \
    --backtitle "$BACKTITLE" \
    --title "$TITLE" \
    --inputbox "$MENU" \
    $LINES $COLUMNS $TEXT\
    2>&1 >/dev/tty
}
# ### ### #
# ### File checker ### #
check_file_provided () {
echo "You provided those file to compile : $@. Are they correct ?"
  read -r -p "" response
  case $response in
    [nN][oO]|[nN])
      echo -e "$red Please correct the file.$basic"
      TO_COMPILE="$(init_inputbox "Haxe Compilation Helper" "FILES TO COMPUTE" "Enter the file you wish to compile -- default dev.hxml testing.hxml prep.hxml prod.hxml" 16 40 "dev.hxml testing.hxml prep.hxml prod.hxml")"
      if [ -z $TO_COMPILE ]
        then
          echo -e "$red You did not provide any file. Now exiting the process. :'( $basic"
          exit 1
      fi
     ;;
    *)
      TO_COMPILE="$@"
      ;;
  esac
}
# ### ### #
# ------------------------------------------------------------------------------
# Getting args -----------------------------------------------------------------
eval set -- "$TEMP"
while true
do
  case "${1}" in
    -h|--help)
      echo "$HELP_MESSAGE"; exit 0;;
    -q|--quiet)
      quiet=0;shift;;
    -y|--non-interactive)
      interactive=1;shift;;
    -w|--watch)
      watched=0;shift;;
    --) shift; break;;
    *) echo "You provided a wrong option"; echo $HELP_MESSAGE; exit 1;;
  esac
done
# ------------------------------------------------------------------------------
# Starting process -------------------------------------------------------------
echo "Welcome little guy. I'll help you to compile Avast IPS project using my small whale friend :) "
# ### Check or Get file to compiles ### #
if [ $# -eq 0 ] && [ $interactive -eq 0 ]
then
  echo "First, please enter the file you want to compile :)"
  TO_COMPILE="$(init_inputbox "Haxe Compilation Helper" "FILES TO COMPUTE" "Enter the file you wish to compile -- default dev.hxml testing.hxml prep.hxml prod.hxml" 16 40 "$DEFAULT")"
  check_file_provided $TO_COMPILE
else
  if [ $# -eq 0 ]
  then
    TO_COMPILE="$DEFAULT"
  else
    if [ $interactive -eq 0 ]
      then
      check_file_provided $@
    else
      TO_COMPILE="$@"    
    fi
  fi
fi
# ### ### #
echo -e "$green Ok, we are now going to proceed to the compilation :) $basic"
# ### Get os to adapat command ### #
OS=$(uname -s)
if [ $watched -eq 0 ]
  then
  if [[ $OS == "Linux*" ]]
    then
      CMD="docker run --rm -it --user $(id -u) --name haxe_compiler --entrypoint=watcher -v $(pwd):/home/haxe  -v $(pwd)/haxe:/home/compile $DOCKER_NAME_SPACE/haxe_compiler:$HAXE_COMPILER_TAG $TO_COMPILE"
    else
      CMD="docker run --rm -it --name haxe_compiler --entrypoint=watcher -v $(pwd):/home/haxe  -v $(pwd)/haxe:/home/compile $DOCKER_NAME_SPACE/haxe_compiler:$HAXE_COMPILER_TAG $TO_COMPILE"
  fi
else
  if [[ $OS == "Linux*" ]]
    then
      CMD="docker run --rm -it --user $(id -u) --name haxe_compiler -v $(pwd):/home/haxe  -v $(pwd)/haxe:/home/compile $DOCKER_NAME_SPACE/haxe_compiler:$HAXE_COMPILER_TAG  $TO_COMPILE"
    else
      CMD="docker run --rm -it --name haxe_compiler -v $(pwd):/home/haxe -v $(pwd)/haxe:/home/compile $DOCKER_NAME_SPACE/haxe_compiler:$HAXE_COMPILER_TAG  $TO_COMPILE"
  fi
fi
# ### ### #
# ### Run the compiler container ### #
if [[ $quiet -eq 1 ]]
  then
    time $CMD
  else 
    time $CMD > /dev/null 1> /dev/null 2> /dev/null
fi
# ### ### #
# ### Exit ### #
if [[ $? -eq 0 ]]
  then
    echo -e "$green The process went well, your project is now compiled. See you again :) $basic"
  else
    CODE=$? 
    echo -e "$red Process failed. Check what goes wrong and try it again :( $basic"
    exit $CODE
fi
# ------------------------------------------------------------------------------
